/*
 * FieldLengthVerifier.java, 2005-10-19
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               FieldLengthVerifier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.validators;

import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

/**
 * Limit a field to a given number of characters
 * 
 * @author Johan Cwiklinski
 * @version 1.0
 */
public class FieldLengthVerifier extends DefaultStyledDocument{
	private static final long serialVersionUID = 6194104918846820459L;
	private int longueur;
	
	/**
	 * Limit the field to the number of characters
	 * 
	 * @param caracts max number of characters
	 */
	public FieldLengthVerifier(int caracts){
		this.longueur = caracts;
	}

	/**
	 * @param offs
	 * @param str
	 * @param a
	 * @throws BadLocationException if field length is greater than the one specified
	 */
	@Override
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException{
		if( (getLength() + str.length()) <= longueur )
			super.insertString(offs, str, a);
		else
			Toolkit.getDefaultToolkit().beep();
	}
}
