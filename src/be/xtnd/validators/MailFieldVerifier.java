/*
 * MailFieldVerifier.java, 2005-09-21
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MailFieldVerifier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.validators;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.commons.validator.EmailValidator;

import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Check a mail adress validity.
 * Based on apache commons validator API.
 *
 * @author Johan Cwiklinski
 * @version 1.0
 */
public class MailFieldVerifier extends InputVerifier{
	private String mail_value;
	
	/**
	 * Default constructor
	 * 
	 * @param mail mail address to check
	 * @see InputVerifier
	 */
	public MailFieldVerifier(String mail) {
		this.mail_value = mail;
	}

	/**
	 * Check the address
	 * 
	 * @param input input component
	 * @return true if mail is valid, false otherwise
	 * @see EmailValidator
	 */
	@Override
	public boolean verify(JComponent input) {
		 if (input instanceof JTextField) {
		 	EmailValidator val = EmailValidator.getInstance();
		 	JTextField field = (JTextField)input;
		 	if(!val.isValid(field.getText()) && !field.getText().equals("")){
          		int response = JOptionPane.showConfirmDialog(
          				MainGui.desktop, 
          				CommonsI18n.tr("Entered mail adress is not valid.\nDo you want to change its value?\nIf you choose 'no', this will revert to the initial value."),
          				CommonsI18n.tr("Error!"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
          		if(response==JOptionPane.NO_OPTION){
         			field.setText(mail_value);
          			return true;
          		}
			 	return val.isValid(field.getText());
		 	}
		 }
		 return true;
	}
	
	/**
	 * Does component should release focus?
	 * 
	 * @param input input component
	 * @return true if mail is valid, false otherwise
	 */
	@Override
    public boolean shouldYieldFocus(JComponent input) {
          return verify(input);
    }	
}
