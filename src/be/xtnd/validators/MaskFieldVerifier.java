/*
 * MaskFieldVerifier.java, 2005-09-21
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MaskFieldVerifier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.validators;

import java.text.ParseException;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField.AbstractFormatter;

import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Check the given value against given mask.
 * Warning, this seems to work as expected only when <code>MaskFormatter</code>
 * does not contains unneeded literal characters except spaces (ie. a book
 * ISBN: #-###-#####-#). In these cases, if the <code>FormattedTextField</code>
 * is empty, value will always be composed of unneeded characters ('-' for ISBN).
 *
 * @author Johan Cwiklinski
 * @version 1.0
 */
public class MaskFieldVerifier extends InputVerifier {
    private String the_value, mask_value;
    private String message;
    
    /**
     * Default constructor.
     * This will initialize check of the passed value. A standard
     * message will be displayed on error.  
     * 
     * @param the_value string to check
     * @param mask_value mask formatter
     */
	public MaskFieldVerifier(String the_value, String mask_value) {
		this.the_value = the_value;
		this.mask_value = mask_value;
		this.message = CommonsI18n.tr(
				"Incorrect format ({0}).\nDo you want to change the value?\nIf you choose \"no\", current incorrect value will be erased.",
				this.mask_value
		);
	}
	
    /**
     * Default constructor.
     * This will initialize check of the passed value. The given 
     * error message will be displayed on error.
     * 
     * @param the_value string to check
     * @param mask_value mask formatter
     * @param message the localized and parameted error message to show
     */	
	public MaskFieldVerifier(String the_value, String mask_value, String message){
		this.the_value = the_value;
		this.mask_value = mask_value;
		this.message = message;
	}
	
	/**
	 * Check content matches mask
	 * 
	 * @param input input component
	 * @return true if matches, false otherwise
	 */
	@Override
	public boolean verify(JComponent input) {
        if (input instanceof JFormattedTextField) {
            JFormattedTextField ftf = (JFormattedTextField)input;
            AbstractFormatter formatter = ftf.getFormatter();
            if (formatter != null) {
                String text = ftf.getText();
                if(!text.trim().equals("")){
	                 try {
	                      formatter.stringToValue(text);
	                      return true;
	                  } catch (ParseException pe) {
	                  		int response = JOptionPane.showConfirmDialog(
	                  				MainGui.desktop, 
									this.message,
									CommonsI18n.tr("Error!"),
									JOptionPane.YES_NO_OPTION,
									JOptionPane.WARNING_MESSAGE);
	                  		if(response==JOptionPane.NO_OPTION){
	                  			if(the_value.trim().equals("")){
	                  				ftf.setText(null);
	                  				ftf.setValue(null);
	                  			}else if(!the_value.trim().equals("")){
	                  				ftf.setText(the_value);
	                  			}else if(ftf.getValue()!=null){
	                  				ftf.setText(ftf.getValue().toString());
	                  			} 
	                  			return true;
	                  		}
	                  		return false;
	                  }
                }else{
                	ftf.setValue(null);
                }
             }
         }
         return true;
     }
	
	/**
	 * Should component release focus?
	 * 
	 * @param input input component
	 * @return true if value matches mask, false otherwise
	 */
	@Override
    public boolean shouldYieldFocus(JComponent input) {
         return verify(input);
    }
}