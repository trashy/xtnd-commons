/*
 * ApplicationProperties.java, 2010-03-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2010 Johan Cwiklinski
 *
 * File :               ApplicationProperties.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Usage :
 * <code>
 * public class ApplicationProperties extends be.xtnd.commons.ApplicationProperties {
 *     private static final String BUNDLE_NAME = "path-to-messages";
 *  
 *     public static String getMsg(String key){
 *         return getString(key, BUNDLE_NAME);
 *     }
 * }
 * </code>
 *
 * @author Johan Cwiklinski
 * @since 2005-10-19
 */
abstract public class ApplicationProperties {
	static Logger logger = Logger.getLogger(ApplicationProperties.class.getName());

	/**
	 * Search and return a key value from application properties file
	 * 
	 * @param key key to look for
	 * @param bundle path to resource file
	 * @return key value in the resource file
	 */
	protected static String getString(String key, String bundle) {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {
			return getBundle(bundle).getString(key);
		} catch (MissingResourceException e) {
			logger.warn("missing property key : " + key + " - in file : " + bundle);
			return '!' + key + '!';
		}
	}

	/**
	 * Get named bundle
	 * @param name
	 * @return
	 */
	protected static ResourceBundle getBundle(String name){
		ResourceBundle rb = ResourceBundle.getBundle(name);
		return rb;
	}

}
