/*
 * MDIDesktopPane.java, 2005-06-04
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MDIDesktopPane.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

/**
 * JInternalFrame management
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-04
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.beans.PropertyVetoException;

import javax.swing.DefaultDesktopManager;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

/**
 * A <code>JDesktopPane</code> that implements scroll bars
 * (vertical and horizontal).
 * 
 * @see JDesktopPane
 * @see JScrollPane
 */
public class MDIDesktopPane extends JDesktopPane {
	private static final long serialVersionUID = -3613806668421704879L;
	private static int FRAME_OFFSET=20;
    private MDIDesktopManager manager;
    Image image;
    boolean enableFond = false;

    /**
     * Create a new <code>MDIDesktopPane</code>
     */
    public MDIDesktopPane() {
        manager=new MDIDesktopManager(this);
        setDesktopManager(manager);
        setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
    }
    
    /**
     * Create a new <code>MDIDesktopPane</code> 
     * with a background image
     * 
     * @param fond background image
     */
    public MDIDesktopPane(Image fond) {
        manager=new MDIDesktopManager(this);
        setDesktopManager(manager);
        setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
    	this.enableFond = true;
    	this.image = fond;
    }
    
    /**
     * Draws components
     * 
     * @param g Graphics
     */
    @Override
	public void paintComponent(Graphics g) {
        if(this.enableFond){
        	super.paintComponent(g);
        	int posX = (getWidth()-image.getWidth(null))/2;
        	int posY = (getHeight()-image.getHeight(null))/2;
        	g.drawImage(image,posX,posY,this );
        }
     }  

	/**
	 * Set sizes
	 * 
	 * @param x 
	 * @param y
	 * @param w
	 * @param h
	 */
	@Override
    public void setBounds(int x, int y, int w, int h) {
        super.setBounds(x,y,w,h);
        checkDesktopSize();
    }

    /**
     * Add a <code>JInternalFrame</code>
     * 
     * @param frame <code>JInternalFrame</code> to add
     * @return Component
     */
    public Component add(JInternalFrame frame) {
        JInternalFrame[] array = getAllFrames();
        Point p;

        Component retval=super.add(frame);
        checkDesktopSize();
        if (array.length > 0) {
            p = array[0].getLocation();
            p.x = p.x + FRAME_OFFSET;
            p.y = p.y + FRAME_OFFSET;
        }
        else {
            p = new Point(10, 10);
        }

        moveToFront(frame);
        frame.setVisible(true);
        try {
            frame.setSelected(true);
        } catch (PropertyVetoException e) {
            frame.toBack();
        }
        return retval;
    }

    /**
     * Delete a <code>Component</code>
     * @param c Component
     */
    @Override
    public void remove(Component c) {
        super.remove(c);
        checkDesktopSize();
    }

    /**
     * Display frames cascading
     */
    public void cascadeFrames() {
        int x = 0;
        int y = 0;
        JInternalFrame allFrames[] = getAllFrames();

        manager.setNormalSize();
        int frameHeight = (getBounds().height - 5) - allFrames.length * FRAME_OFFSET;
        int frameWidth = (getBounds().width - 5) - allFrames.length * FRAME_OFFSET;
        for (int i = allFrames.length - 1; i >= 0; i--) {
            allFrames[i].setSize(frameWidth,frameHeight);
            allFrames[i].setLocation(x,y);
            x = x + FRAME_OFFSET;
            y = y + FRAME_OFFSET;
        }
    }

    /**
     * Displays frames mosaic
     */
    public void tileFrames() {
        java.awt.Component allFrames[] = getAllFrames();
        manager.setNormalSize();
        int frameHeight = getBounds().height/allFrames.length;
        int y = 0;
        for (int i = 0; i < allFrames.length; i++) {
            allFrames[i].setSize(getBounds().width,frameHeight);
            allFrames[i].setLocation(0,y);
            y = y + frameHeight;
        }
    }

    /**
     * Set components sizes (maximum, preferred, minimum) to the
     * specified dimension.
     * 
     * @param d components dimensions
     * 
     */
    public void setAllSize(Dimension d){
        setMinimumSize(d);
        setMaximumSize(d);
        setPreferredSize(d);
    }

    /**
     * Set components sizes (maximum, preferred, minimum) to the
     * specified width an height.
     * 
     * @param width width
     * @param height height
     */
    public void setAllSize(int width, int height){
        setAllSize(new Dimension(width,height));
    }

    private void checkDesktopSize() {
        if (getParent()!=null&&isVisible()) manager.resizeDesktop();
    }
}

/**
 * Replacement for standard <code>DesktopManager</code>.
 * Used to get scrollbars support.
 */
class MDIDesktopManager extends DefaultDesktopManager {
	private static final long serialVersionUID = 5211888441095319041L;
	private MDIDesktopPane desktop;

	/**
	 * 
	 * @param desktop
	 */
    public MDIDesktopManager(MDIDesktopPane desktop) {
        this.desktop = desktop;
    }
    /**
     * @param f JComponent
     */
    @Override
    public void endResizingFrame(JComponent f) {
        super.endResizingFrame(f);
        resizeDesktop();
    }

    /**
     * @param f JComponent
     */
    @Override
    public void endDraggingFrame(JComponent f) {
        super.endDraggingFrame(f);
        resizeDesktop();
    }

    /**
     * 
     *
     */
    public void setNormalSize() {
        JScrollPane scrollPane=getScrollPane();
        int x = 0;
        int y = 0;
        Insets scrollInsets = getScrollPaneInsets();

        if (scrollPane != null) {
            Dimension d = scrollPane.getVisibleRect().getSize();
            if (scrollPane.getBorder() != null) {
               d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right,
                         d.getHeight() - scrollInsets.top - scrollInsets.bottom);
            }

            d.setSize(d.getWidth() - 20, d.getHeight() - 20);
            desktop.setAllSize(x,y);
            scrollPane.invalidate();
            scrollPane.validate();
        }
    }

    private Insets getScrollPaneInsets() {
        JScrollPane scrollPane=getScrollPane();
        if (scrollPane==null) return new Insets(0,0,0,0);
        else return getScrollPane().getBorder().getBorderInsets(scrollPane);
    }

    private JScrollPane getScrollPane() {
        if (desktop.getParent() instanceof JViewport) {
            JViewport viewPort = (JViewport)desktop.getParent();
            if (viewPort.getParent() instanceof JScrollPane)
                return (JScrollPane)viewPort.getParent();
        }
        return null;
    }

    protected void resizeDesktop() {
        int x = 0;
        int y = 0;
        JScrollPane scrollPane = getScrollPane();
        Insets scrollInsets = getScrollPaneInsets();

        if (scrollPane != null) {
            JInternalFrame allFrames[] = desktop.getAllFrames();
            for (int i = 0; i < allFrames.length; i++) {
                if (allFrames[i].getX()+allFrames[i].getWidth()>x) {
                    x = allFrames[i].getX() + allFrames[i].getWidth();
                }
                if (allFrames[i].getY()+allFrames[i].getHeight()>y) {
                    y = allFrames[i].getY() + allFrames[i].getHeight();
                }
            }
            Dimension d=scrollPane.getVisibleRect().getSize();
            if (scrollPane.getBorder() != null) {
               d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right,
                         d.getHeight() - scrollInsets.top - scrollInsets.bottom);
            }

            if (x <= d.getWidth()) x = ((int)d.getWidth()) - 20;
            if (y <= d.getHeight()) y = ((int)d.getHeight()) - 20;
            desktop.setAllSize(x,y);
            scrollPane.invalidate();
            scrollPane.validate();
        }
    }
}
