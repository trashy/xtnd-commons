/*
 * GuiCommons.java, 2005-06-14
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               GuiCommons.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

import be.xtnd.commons.gui.AboutBox;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.Options;

/**
 * Common GUI utilities.
 * Implements methods that mainly permit to populate components
 * (such as <code>JMenu</code>, <code>JMenuItem</code>,
 * <code>JButton</code>, <code>JComboBox</code>, ...)
 * 
 * @author Johan Cwiklinski
 * @since 2006-06-14
 */
public class GuiCommons {

	private int longueur;
	private static JLabel license, title, url;
	@Deprecated
	private ResourceBundle bundle;

	/** 
	 * Default constructor, mainly used by {@link Config.ConfigWindow ConfigWindow} 
	 */
	public GuiCommons(){}
	
	/**
	 * Default constructor
	 * @param bundle ResourceBundle à lire
	 * @deprecated since we use xgettext-commons
	 */
	public GuiCommons(ResourceBundle bundle){
		this.bundle = bundle;
	}
	
	/**
	 * Crée une barre de deux bouttons (Ok et Annuler).
	 * Définit le bouton Ok par défaut, attribue les 
	 * <code>ActionListener</code>
	 * 
	 * @param frame JInternalFrame à laquelle doivent être ajoutés les boutons
	 * @param okayEvent listener du bouton ok
	 */
	public void createButtonBar(JInternalFrame frame, ActionListener okayEvent){
		FormPanel pane = new FormPanel( "be/xtnd/commons/gui/descriptions/buttons_bar.jfrm" );

		JButton okay = (JButton)pane.getButton("okay");
		okay.setText(CommonsI18n.tr("Okay"));
		okay.addActionListener(okayEvent);
		frame.getRootPane().setDefaultButton(okay);
		
		JButton cancel = (JButton)pane.getButton("cancel");
		cancel.setText(CommonsI18n.tr("Cancel"));
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				MainGui.desktop.getSelectedFrame().dispose();
			}
		});
		frame.getContentPane().add(pane,BorderLayout.SOUTH);
	}
		
	/**
	 * Create a header with passed logo. Used into splash screen
	 * and about windows. 
	 * 
	 * @param pane <code>FormPanel</code> to format
	 * @param logo logo to display
	 * @param bundle ResourceBundle à lire
	 */
	public static void createHeader(FormPanel pane, String logo, ResourceBundle bundle){
		license = pane.getLabel("license");
		license.setText(bundle.getString("appli.copyright"));
		
		title = pane.getLabel("title");
		title.setText(bundle.getString("appli.name")+" v"+bundle.getString("appli.version"));
		
		url = pane.getLabel("url");
		url.setText(bundle.getString("appli.url"));
		
		JLabel image = pane.getLabel("image");
		image.setIcon(new ImageIcon(ClassLoader.getSystemResource(bundle.getString("appli."+logo)))); //$NON-NLS-1$
	}
	
	/** FIXME 0.0.2 : remove */
	/**
	 * Limite la zone au nombre de caractères <code>caracts</code>
	 * 
	 * @param caracts : nombre de caractères de la limite
	 * 
	 * @return doc -> JTextField.setDocument(doc)
	 * 
	 * @see DefaultStyledDocument
	 * 
	 * @deprecated lors d'une utilisation multiple, la valeur est toujours la dernière.
	 * Remplacé par {@link be.xtnd.validators.FieldLengthVerifier#FieldLengthVerifier(int) FieldLengthVerifier}.
	 * 
	 */
	@Deprecated
	public DefaultStyledDocument doc(int caracts){
		this.longueur = caracts;
		DefaultStyledDocument docu = new DefaultStyledDocument(){
			private static final long serialVersionUID = -6870710569372996547L;

			public void insertString(int offs, String str, AttributeSet a)
			throws BadLocationException{
				System.err.println("size : " + longueur);
				if( (getLength() + str.length()) <= longueur ){
					System.err.println("throws correctly exception");
					super.insertString(offs, str, a);
				}else{
					System.err.println("error occured during doc sized");
					Toolkit.getDefaultToolkit().beep();
				}
			}
		};
		return docu;
	}

	/**
	 * Creates a new<code>JComboBox</code> using
	 * given <code>ArrayList</code>.
	 * 
	 * @param list <code>ArrayList</code> values
	 * 
	 * @see JComboBox
	 * @see ArrayList
	 * 
	 * @return new <code>JComboBox</code>
	 */
	public JComboBox buildNewCombo(ArrayList<?> list){
		JComboBox box = new JComboBox();
		this.buildCombo(box, list);
		return box;
	}

	/**
	 * Populate a <code>JComboBox</code> using given 
	 * <code>ArrayList</code>.
	 * 
	 * @param box <code>JComboBox</code> to populate
	 * @param list <code>ArrayList</code> contenant les valeurs
	 * 
	 * @see JComboBox
	 * @see ArrayList
	 */
	public void buildCombo(JComboBox box, ArrayList<?> list){
		box.addItem(CommonsI18n.tr("- Select -"));
		for(int i = 0; i<list.size(); i++){
			box.addItem(list.get(i));
		}
	}

	/**
	 * Creates a menu entry
	 * 
	 * @param menu original menu
	 * @param i18n locale reference
	 * @param icon menu's image
	 * 
	 * @see JMenu
	 * 
	 * @return JMenu
	 * @deprecated {@link #menuEntry(JMenu, String, String, Icon)}
	 */
	public JMenu menuEntry(JMenu menu, String i18n, Icon icon){
		System.out.println("  Menu "+bundle.getString(i18n));
		menu.setMnemonic(bundle.getString(i18n+".mnemonic").charAt(0));
		menu.setText(bundle.getString(i18n));
		menu.setIcon(icon);	
		return menu;
	}

	/**
	 * Creates a menu entry
	 * 
	 * @param menu original menu
	 * @param text localized text for the menu entry
	 * @param mnemonic localized mnemonic character 
	 * @param icon menu's image
	 * 
	 * @see JMenu
	 * 
	 * @return JMenu
	 */
	public JMenu menuEntry(JMenu menu, String text, String mnemonic, Icon icon){
		System.out.println(CommonsI18n.tr("  Menu \"{0}\"", text)); 
		menu.setMnemonic(mnemonic.charAt(0));
		menu.setText(text);
		menu.setIcon(icon);	
		return menu;
	}
	

	/**
	 * Crée une entrée de sous menu
	 * 
	 * @param item item à modifier
	 * @param icon image de l'item
	 * @param i18n référence de la locale
	 * @param keystroke Raccourci clavier
	 * 
	 * @see JMenuItem
	 * 
	 * @return JMenuItem
	 * @deprecated {@link #submenuEntry(JMenuItem, Icon, String, String, String, KeyStroke)}
	 */
	public JMenuItem submenuEntry(JMenuItem item, Icon icon, String i18n, KeyStroke keystroke){
		System.out.println("     Sous menu "+bundle.getString(i18n));
	    item.setToolTipText(bundle.getString(i18n+".tooltip"));
	    item.setIcon(icon);
	    item.setMnemonic(bundle.getString(i18n+".mnemonic").charAt(0));
	    item.setText(bundle.getString(i18n));
	    item.setAccelerator(keystroke);
	    return item;
	}
	
	/**
	 * Creates a sub-menu entry
	 * 
	 * @param item Original JMenuItem
	 * @param icon item icon
	 * @param text localized text for the entry
	 * @param tooltip localized tooltip text
	 * @param mnemonic localized mnemonic character 
	 * @param keystroke keystroke
	 * 
	 * @see JMenuItem
	 * 
	 * @return JMenuItem
	 */
	public JMenuItem submenuEntry(JMenuItem item, Icon icon, String text, String tooltip, String mnemonic, KeyStroke keystroke){
		System.out.println(CommonsI18n.tr("     Sub-menu \"{0}\"", text)); 
	    item.setToolTipText(tooltip);
	    item.setIcon(icon);
	    item.setMnemonic(mnemonic.charAt(0));
	    item.setText(text);
	    item.setAccelerator(keystroke);
	    return item;
	}
	
	/**
	 * Crée un sous menu déroulant
	 * 
	 * @param item sous menu
	 * @param icon image du sous menu
	 * @param i18n référence de la locale
	 * 
	 * @see JMenu
	 * 
	 * @return JMenu
	 * @deprecated {@link #submenuEntry(JMenu, Icon, String, String)}
	 */
	public JMenu submenuEntry(JMenu item, Icon icon, String i18n){
		System.out.println("     Sous menu "+bundle.getString(i18n)); 
	    item.setIcon(icon);
	    item.setMnemonic(bundle.getString(i18n+".mnemonic").charAt(0));
	    item.setText(bundle.getString(i18n));
	    return item;
	}
	
	/**
	 * Creates a rolling sub-menu
	 * 
	 * @param item sub-menu
	 * @param icon sub-menu image
	 * @param text localized text for the entry
	 * @param mnemonic localized mnemonic character 
	 * 
	 * @see JMenu
	 * 
	 * @return JMenu
	 */
	public JMenu submenuEntry(JMenu item, Icon icon, String text, String mnemonic){
		System.out.println(CommonsI18n.tr("     Sub-menu \"{0}\"", text)); 
	    item.setIcon(icon);
	    item.setMnemonic(mnemonic.charAt(0));
	    item.setText(text);
	    return item;
	}
	
	/** FIXME 0.0.2 : check if this method is used in one of the projects */
	/**
	 * Ajuste les propriétés du bouton en paramètre
	 * 
	 * @param button bouton à modifier
	 * @param i18n référence de la locale
	 * @param icon icône associée
	 * 
	 * @see JButton
	 * 
	 * @return JButton
	 * @deprecated {@link #buildCommonButton(JButton, String, String, String, Icon)}
	 * Semmes that this method has not been used for ages... To check
	 */
	public JButton buildButton(JButton button, String i18n, Icon icon){
		Options.setUseNarrowButtons(Boolean.TRUE);
	  	button.setIcon(icon);
	  	button.setText(bundle.getString(i18n));
	  	button.setToolTipText(bundle.getString(i18n+".tooltip"));
	  	button.setMnemonic(bundle.getString(i18n+".mnemonic").charAt(0));
	  	return button;
	}
		
	/**
	 * Ajuste les propriétés du bouton en paramètre
	 * Utilisé pour les boutons généraux des packages 
	 * be.xtnd.commons.*
	 * 
	 * @param button bouton à modifier
	 * @param i18n référence de la locale
	 * @param icon icône associée
	 * 
	 * @see JButton
	 * 
	 * @return JButton
	 * @deprecated Use {@link #buildCommonButton(JButton, String, String, String, Icon)} instead
	 */
	public JButton buildCommonButton(JButton button, String i18n, Icon icon){
		Options.setUseNarrowButtons(Boolean.TRUE);
	  	button.setIcon(icon);
	  	button.setText(MessagesCommons.getString(i18n));
	  	button.setToolTipText(MessagesCommons.getString(i18n+".tooltip"));
	  	button.setMnemonic(MessagesCommons.getString(i18n+".mnemonic").charAt(0));
	  	return button;		
	}

	/**
	 * Adjust properties for given button. Used for generic buttons
	 * from <code>be.xtnd.commons.*</code>
	 * 
	 * @param button button to edit
	 * @param text localized text for the button
	 * @param tooltip localized tooltip text
	 * @param mnemonic localized mnemonic character 
	 * @param icon associated icon
	 * 
	 * @see JButton
	 * 
	 * @return JButton
	 */
	public JButton buildCommonButton(JButton button, String text, String tooltip, String mnemonic, Icon icon){
		Options.setUseNarrowButtons(Boolean.TRUE);
	  	button.setIcon(icon);
	  	button.setText(text);
	  	button.setToolTipText(tooltip);
	  	button.setMnemonic(mnemonic.charAt(0));
	  	return button;		
	}

	/**
	 * Ajuste les propriétés du bouton en paramètre
	 * pour affichage sur la toolbar
	 * 
	 * @param button bouton à modifier
	 * @param i18n référence de la locale
	 * @param icon icône associée
	 * 
	 * @see JButton
	 * 
	 * @return JButton
	 * @deprecated {@link #buildBarButton(JButton, String, String, Icon)}
	 */
	public JButton buildBarButton(JButton button, String i18n, Icon icon){
		Options.setUseNarrowButtons(Boolean.TRUE);
	  	button.setIcon(icon);
	  	button.setToolTipText(bundle.getString(i18n+".tooltip"));
	  	button.setMnemonic(bundle.getString(i18n+".mnemonic").charAt(0));
	  	return button;
	}

	/**
	 * Set properties on toolbar buttons
	 * 
	 * @param button original button
	 * @param tooltip localized tooltip text
	 * @param mnemonic localized mnemonic character 
	 * @param icon associated icon
	 * 
	 * @see JButton
	 * 
	 * @return JButton
	 */
	public JButton buildBarButton(JButton button, String tooltip, String mnemonic, Icon icon){
		Options.setUseNarrowButtons(Boolean.TRUE);
	  	button.setIcon(icon);
	  	button.setToolTipText(tooltip);
	  	button.setMnemonic(mnemonic.charAt(0));
	  	return button;
	}

	/**
	 * Build the "about" menu entry, and adds its action
	 * @param bundle Caller bundle passed to {@link AboutBox}
	 * @return JMenuItem
	 */
	public JMenuItem aboutMenuEntry(ResourceBundle bundle){
		JMenuItem about = new JMenuItem();
		
		this.submenuEntry(
				about,
				null,
				CommonsI18n.tr("About..."),
				CommonsI18n.tr("About {0}...", bundle.getString("appli.name")),
				CommonsI18n.trc("About menu mnemonic", "a"),
				null
		);
		about.addActionListener(new AboutAction(bundle));

		return about;
	}

	/**
	 * Build the "quit" menu entry, and adds its action
	 * @param bundle Caller bundle
	 * @return JMenuItem
	 */
	public JMenuItem quitMenuEntry(ResourceBundle bundle){
		JMenuItem quit = new JMenuItem();
		
		this.submenuEntry(
				quit,
				null,
				CommonsI18n.tr("Quit"),
				CommonsI18n.tr("Quit {0}", bundle.getString("appli.name")),
				CommonsI18n.trc("Quit menu mnemonic", "q"),
			    KeyStroke.getKeyStroke(KeyEvent.VK_Q, Event.CTRL_MASK)
		);
		quit.addActionListener(new QuitAction());

		return quit;
	}

	/**
	 * Build the "configuration" menu entry, and adds its action
	 * @param bundle Caller bundle
	 * @return JMenuItem
	 */
	public JMenuItem configMenuEntry(ResourceBundle bundle, MainGui parent, Icon icon){
		JMenuItem config = new JMenuItem();
		
		this.submenuEntry(
				config,
				icon,
				CommonsI18n.tr("Configuration"),
				CommonsI18n.tr("Configure {0}...", bundle.getString("appli.name")),
				CommonsI18n.trc("Configuration menu mnemonic", "c"),
			    KeyStroke.getKeyStroke(KeyEvent.VK_E, Event.CTRL_MASK)
		);
		config.addActionListener(new ConfigAction(parent));
		
		return config;
	}

	/**
	 * Action on about
	 * @author trasher
	 * @since 2010-03-30
	 */
	class AboutAction implements ActionListener {
		private ResourceBundle bundle;

		/**
		 * Default constructor
		 * @param bundle Caller bundle 
		 */
		public AboutAction(ResourceBundle bundle){
			super();
			this.bundle = bundle;
		}

		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(AboutBox.NAME, null)){
				new AboutBox(this.bundle);
			}
		}
	}

	/**
	 * Action on quit
	 * @author trasher
	 * @since 2010-03-30
	 */
	class QuitAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int response = JOptionPane.showConfirmDialog(
					MainGui.desktop, 
					CommonsI18n.tr("Are you sure you want to exit the application?"),
					CommonsI18n.tr("Quit confirmation"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
    		if (response == JOptionPane.YES_OPTION) {
   			    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
   			    
   			    String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
   			    java.text.SimpleDateFormat sdf = 
   			          new java.text.SimpleDateFormat(DATE_FORMAT);
   			    sdf.setTimeZone(TimeZone.getDefault());          
   			          
   			    System.out.println(CommonsI18n.tr("Application closed on: {0}", sdf.format(cal.getTime())));
    			System.exit(0);
    		}
		}
	}

	/**
	 * Action on configuration
	 * @author trasher
	 * @since 2010-03-30
	 */
	class ConfigAction implements ActionListener {
		private MainGui parent;
		
		public ConfigAction(MainGui parent){
			super();
			this.parent = parent;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(Config.name,null)){
				try {
					MainGui.conf.new ConfigWindow(parent);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}