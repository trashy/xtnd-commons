/*
 * Config.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Config.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulyses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Iterator;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.commons.security.Cryptographie;
import be.xtnd.commons.security.EncryptionClass;

import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * Config will obtain and update configurations options from
 * and to config files located in <code>cantine/install/path/.config</code>.
 * It also creates a default config file if no one exists ; and
 * display configuration window in the GUI.
 *
 * @author Johan CWIKLINKSI
 * @since 2005-05-31
 * @version 1.1
*/
public class Config{
	private String xmlPath;
	private JTextField mysqlServer, mysqlUser, mysqlDb;
	private JTextField [] provider, providerName, providerPort, providerAttributes;
	private JComboBox[] providerMode;
	private JRadioButton[] providerDefault;
	private JPasswordField mysqlPassword, softRootPassword;
	/** Window name */
	public final static String name = "preferences";
	private String sql_provider, sql_server, sql_db, sql_user, sql_password, arguments, mode="",
		root_soft_password, lang, server_name, port, provider_name, crypto_mode = "nomode";
	private List<?> providers;
	private boolean modified=false;
	/** Define if configuration is loaded or not */
	public boolean loaded = false;
	private org.jdom.Document document;
	private Element racine;
	private static final char[] encryptPassPhrase="2]aRF05$ue".toCharArray();
	private Dimension fieldsSize = new Dimension(150,20);
	private JTabbedPane tabbedPane;//tabs container
	private JRadioButton bouncy_crypt, simple_crypt;
	static Logger logger = Logger.getLogger(Config.class.getName());
	
	/**
	 * Build app configuration.
	 * 
	 * @param xmlPath XML file name (msut be into <code>{application}/.config</code> directory)
	 */
	public Config(String xmlPath) {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		this.xmlPath = ".config/" + xmlPath;
	}
	
	/**
	 * Loads configuration from XML file.
	 *  
	 * @param defaultConf <code>boolean</code> define if we have to load a
	 * default configuration file. It's usefull to create a new file if no one 
	 * is present.
	 * With true, it's possible to launch application even if configuration is
	 * broken, or if an important option is missing.  
	 */
	public void loadConfig(boolean defaultConf) {
		logger.debug("*** LOADING PREFERENCES ***");
		Security.addProvider(new BouncyCastleProvider());

        try {                   	
    	    SAXBuilder sxb = new SAXBuilder();
    	    this.document = sxb.build(new File(xmlPath));

    	    this.racine = document.getRootElement();

    		this.providers = racine.getChild("sql_providers").getChildren();
			XPath langPath = XPath.newInstance("//lang[@default='yes']");
			Element langs = (Element) langPath.selectSingleNode(document);
			this.lang = langs.getText()+", file "+langs.getAttributeValue("file")+".jar";

			if(!defaultConf){
	        	XPath providerPath = XPath.newInstance("//sql_provider[@default='yes']");
				Element node = (Element) providerPath.selectSingleNode(document);
				this.provider_name = node.getAttributeValue("name");
				this.sql_provider = node.getText();
				this.port = node.getAttributeValue("port");
				this.server_name = racine.getChild("sql_server").getText();
				this.sql_db = racine.getChild("sql_db").getText();

				this.sql_server = "jdbc:"+provider_name+":";
				mode = node.getAttributeValue("mode");
				if(mode.equalsIgnoreCase("server")){
					this.sql_server += "//";
					if(server_name.length()>0){
						this.sql_server += server_name;
						if(this.port.length()>0) this.sql_server += ":"+this.port;
						this.sql_server += "/";
					}else{
						MainGui.s.end();
						logger.debug("          [Failed]");
						int response = JOptionPane.showConfirmDialog(
								MainGui.desktop, 
								CommonsI18n.tr("Server connection mode requires an host or an IP.\nDo you want to customize your configuration?"),
								CommonsI18n.tr("Server connection"),
								JOptionPane.YES_NO_OPTION,
								JOptionPane.INFORMATION_MESSAGE);
			    		if (response == JOptionPane.YES_OPTION) {
			    			loadConfig(true);
			    			if(MainGui.verifUnicite(name,null)){
			    				//TODO check if null in verifUnicite call does not cause issues
			    				new ConfigWindow(null);
			    			}
			    			return;
			    		}
					}
				}
				this.sql_server += sql_db;
				arguments = node.getAttributeValue("arguments");
				if(arguments.length()>0){
					this.sql_server += arguments; 
				}
				
				this.sql_user = racine.getChild("sql_user").getText();
		        
				this.crypto_mode = racine.getChild("cryptomode").getText();
		        
				if(crypto_mode.equalsIgnoreCase("bouncy")){
					byte[] pass_sql = racine.getChild("sql_password").getText().getBytes();
			        this.sql_password = new String(EncryptionClass.decrypt(
			        		pass_sql,
							encryptPassPhrase));
			        byte[] pass_appli = racine.getChild("root_soft_password").getText().getBytes();
					this.root_soft_password = new String(EncryptionClass.decrypt(
			        		pass_appli,
							encryptPassPhrase));
				}else if(crypto_mode.equalsIgnoreCase("simple")){
					this.sql_password = Cryptographie.decrypte(racine.getChild("sql_password").getText());
					this.root_soft_password = Cryptographie.decrypte(racine.getChild("root_soft_password").getText());
				}
			}
			loaded = true;
			logger.debug("          [Ok]");
		} catch (FileNotFoundException e) {
			MainGui.s.end();
			logger.debug("          [Failed]");
			logger.warn("FileNotFoundException while loading configuration from "+xmlPath);
			Object[] args = {xmlPath};
			int response = JOptionPane.showConfirmDialog(
					MainGui.desktop, 
					CommonsI18n.tr("Configuration file \"{0}\" does not exist.\nDo you want the application to create it?", args),
					CommonsI18n.tr("Configuration error"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
    		if (response == JOptionPane.YES_OPTION) {
    			makeConfig();
    		}else{
    			JOptionPane.showMessageDialog(
    					MainGui.desktop, 
    					CommonsI18n.tr("Application cannot work without configuration file.\nWill exit now."),
						CommonsI18n.tr("Missing file"),
						JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
    			System.exit(0);
    		}
		} catch (JDOMException e) {
			logger.debug("          [Failed]");
			logger.fatal("JDomException while loading configuration : "+e.getMessage());
		} catch (NoSuchProviderException e1) {
			logger.debug("          [Failed]");
			logger.fatal("NoSuchProviderException while loading configuration : "+e1.getMessage());
		} catch (IOException e1) {
			logger.debug("          [Failed]");
			logger.fatal("IOException while loading configuration : "+e1.getMessage());
		} catch (PGPException e1) {
			logger.debug("          [Failed]");
			logger.fatal("PGPException while loading configuration : "+e1.getMessage());
		}
	}
	 
	/**
	 * Build a valid configuration file from the one embedded 
	 * in the package <code>be.xtnd.conf/config-orig.xml</code>. 
	 */
    private void makeConfig(){
			try {
				org.jdom.Document documentOrig;
				SAXBuilder sxb = new SAXBuilder();
				documentOrig = sxb.build(ClassLoader.getSystemResource("be/xtnd/conf/config-orig.xml"));
	            XMLOutputter sortie = new XMLOutputter();
				sortie.output(documentOrig, new FileOutputStream(xmlPath));
				
    			JOptionPane.showMessageDialog(
    					MainGui.desktop, 
						CommonsI18n.tr("Default configuration file has been created.\nYou should now adapt to it your configuration."),
						CommonsI18n.tr("Default configuration"),
						JOptionPane.OK_OPTION+JOptionPane.INFORMATION_MESSAGE);
    			try {
    				loadConfig(true);
    				//TODO check if null in the ConfigWindow call does not cause issues
    				new ConfigWindow(null);
    			} catch (Exception e1) {
					logger.warn(e1.getMessage());
				}
			} catch (JDOMException e) {
				logger.fatal("JDOMException while making config : "+e.getMessage()+" - EXITING");
				System.exit(0);
			} catch (FileNotFoundException e) {
				logger.fatal("FileNotFoundException while making config : "+e.getMessage()+" - EXITING");
				System.exit(0);
			} catch (IOException e) {
				logger.fatal("IOException while making config : "+e.getMessage()+" - EXITING");
				System.exit(0);
			}
    }

    /** FIXME: delete no longer needed argument */ 
    /**
     * Save current configuration in the XML file.
     * 
     * @param @deprecated close true if the window should be closed once configuration
     * have been saved. Should be totally inefficient now. 
     */
    private void saveConfig(boolean close){
		String dialog_message = new String();
		String dialog_titre = new String();
		String dialog_error = new String();
		String sqlPass=null, softPass=null;
    	boolean result=true;		
		int dialog_type = 0;

		try{
			racine.getChild("sql_server").setText(mysqlServer.getText());
			racine.getChild("sql_db").setText(mysqlDb.getText());
			racine.getChild("sql_user").setText(mysqlUser.getText());

			if(bouncy_crypt.isSelected()){
				sqlPass = 
					new String(EncryptionClass.encrypt(
							String.copyValueOf(mysqlPassword.getPassword()).getBytes(), 
							encryptPassPhrase,
							null,
							PGPEncryptedDataGenerator.AES_256, 
							true)
					);
				softPass = 
					new String(EncryptionClass.encrypt(
							String.copyValueOf(softRootPassword.getPassword()).getBytes(), 
							encryptPassPhrase,
							null,
							PGPEncryptedDataGenerator.AES_256, 
							true)
					);		
			}else if(simple_crypt.isSelected()){
				sqlPass = Cryptographie.crypte(new String(mysqlPassword.getPassword()));
				softPass = Cryptographie.crypte(new String(softRootPassword.getPassword()));
			}
			
			racine.getChild("sql_password").setText(sqlPass);

			racine.getChild("root_soft_password").setText(softPass);

			String crypto = (bouncy_crypt.isSelected())?"bouncy":"simple";
			racine.getChild("cryptomode").setText(crypto);
			
			for(int y =0; y < tabbedPane.getTabCount(); y++){
				try {
					XPath providerPath = XPath.newInstance("//sql_provider[@name='"+tabbedPane.getTitleAt(y)+"']");
					Element node = (Element) providerPath.selectSingleNode(document);
					node.getAttribute("name").setValue(providerName[y].getText());
					node.getAttribute("port").setValue(providerPort[y].getText());
					node.getAttribute("mode").setValue(providerMode[y].getSelectedItem().toString());
					node.getAttribute("arguments").setValue(providerAttributes[y].getText());					
					String strdefault = new String("no");
					if(providerDefault[y].isSelected()) strdefault = "yes";
					node.getAttribute("default").setValue(strdefault);
					node.setText(provider[y].getText());   		
				} catch (JDOMException e2) {e2.printStackTrace();}
	    	} 

            XMLOutputter sortie = new XMLOutputter();
			sortie.output(document, new FileOutputStream(xmlPath));
		} catch (FileNotFoundException e) {
			logger.warn("FileNotFoundException saving config : "+e.getMessage()+" - USER ADVICED");
			dialog_error = CommonsI18n.tr("error: ") + e.getMessage();
			result=false;
		} catch (IOException e) {
			logger.warn("IOException saving config : "+e.getMessage()+" - USER ADVICED");
			dialog_error = CommonsI18n.tr("error: ") + e.getMessage();
			result=false;
		} catch (NoSuchProviderException e1) {
			logger.warn("NoSuchProviderException saving config : "+e1.getMessage()+" - USER ADVICED");
			dialog_error = CommonsI18n.tr("error: ") + e1.getMessage();
			result=false;
		} catch (PGPException e1) {
			logger.warn("PGPException saving config : "+e1.getMessage()+" - USER ADVICED");
			dialog_error = CommonsI18n.tr("error: ") + e1.getMessage();
			result=false;
		}

		if(result){
			dialog_message = CommonsI18n.tr("Configuration has been successfully saved.");
			dialog_titre = CommonsI18n.tr("Configuration saved");
			dialog_type = JOptionPane.INFORMATION_MESSAGE;
		}else{
			close=false;
			dialog_message = CommonsI18n.tr("Configuration has not been saved.") + "\n(" + dialog_error + ")";
			dialog_titre = CommonsI18n.tr("Configuration not saved");	
			dialog_type = JOptionPane.ERROR_MESSAGE;
		}
		JOptionPane.showMessageDialog(MainGui.desktop, dialog_message,dialog_titre,dialog_type);
		loadConfig(false);
		if(result && close){
			modified=false;
		}  	
    }
    
    /**
     * Open a window to see/change configuration from the XML file.
     * 
     * 
     * @author Johan Cwiklinski
     * @since 2005-05-31 
     * @version 1.1
     * 
     * @see be.xtnd.commons.gui.EscapeInternalFrame
     * @see Config
     */
    public class ConfigWindow extends EscapeInternalFrame implements ActionListener{
		private static final long serialVersionUID = 8839486849341653324L;
		private JButton okay, annuler, appliquer, simple_crypto_gui;
    	private JLabel serveur_label, db_label, db_user_label, db_passe_label, passe_label, 
			general_label, datas_label, crypt_label;
    	private GuiCommons commons = new GuiCommons();
    	private MainGui parentFrame;
    	
    	/**
    	 * Build configuration window
    	 * 
    	 * @param parent parent MainGui window
    	 */
    	public ConfigWindow(MainGui parent){
    		super();
    		this.parentFrame = parent;
    		super.setTitle(CommonsI18n.tr("Application configuration"));
            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
    		FormPanel pane = new FormPanel( "be/xtnd/commons/gui/descriptions/preferences.jfrm" );

    		initLabels(pane);
    		
			mysqlServer = pane.getTextField("serveur");
			configFields(mysqlServer, server_name);
			mysqlServer.setCaretPosition(0);


			mysqlPassword = (JPasswordField)pane.getTextField("base_passe");
			configFields(mysqlPassword, sql_password);
			
	    	softRootPassword = (JPasswordField)pane.getTextField("appli_passe");
			configFields(softRootPassword, root_soft_password);

			mysqlUser = pane.getTextField("base_user");
			configFields(mysqlUser, sql_user);
			
			mysqlDb = pane.getTextField("base");
			configFields(mysqlDb, sql_db);
			
			simple_crypt = pane.getRadioButton("simple_crypt");
			simple_crypt.setText(CommonsI18n.tr("simple"));
			simple_crypt.setToolTipText(CommonsI18n.tr("Simple encryption alogithm"));
			if(crypto_mode.equalsIgnoreCase("simple")) simple_crypt.setSelected(true);
			simple_crypt.addActionListener(this);
			
			bouncy_crypt = pane.getRadioButton("bouncy_crypt");
			bouncy_crypt.setText(CommonsI18n.tr("bouncy"));
			bouncy_crypt.setToolTipText(CommonsI18n.tr("Encryption algorithm based on BouncyCastle API (requires unrestricted JCE)"));
			if(crypto_mode.equalsIgnoreCase("bouncy")) bouncy_crypt.setSelected(true);
        	bouncy_crypt.addActionListener(this);

			simple_crypto_gui = (JButton)pane.getButton("simple_crypto_gui");
			simple_crypto_gui.setText(CommonsI18n.tr("GUI"));
			simple_crypto_gui.setToolTipText(CommonsI18n.tr("A GUI for simple encryption algorithm."));
			simple_crypto_gui.addActionListener(new SimpleClik());
			
    		okay = (JButton)pane.getButton("okay");
    		//commons.buildCommonButton(okay, "window.config.buttons.ok", null);
    		commons.buildCommonButton(
    				okay,
    				CommonsI18n.tr("Okay"),
    				CommonsI18n.trc("OK Button (tooltip)", "Save configuration and close window"),
    				CommonsI18n.trc("OK Button (mnemonic)", "o"),
					null
			);
    		okay.addActionListener(new OkClik());
    		
    		appliquer = (JButton)pane.getButton("appliquer");
    		commons.buildCommonButton(
    				appliquer,
    				CommonsI18n.tr("Apply"),
    				CommonsI18n.trc("Apply button (tooltip)", "Apply configuration without closing panel"),
    				CommonsI18n.trc("Apply button (mnemonic)", "a"),
    				null
			);
    		appliquer.addActionListener(new ApplyClik());

    		annuler = (JButton)pane.getButton("annuler");
    		commons.buildCommonButton(
    				annuler,
    				CommonsI18n.tr("Cancel"),
    				CommonsI18n.trc("Cancel button (tooltip)", "Close panel without saving changes"),
    				CommonsI18n.trc("Cancel button (mnemonic)", "c"),
					null
			);
    		annuler.addActionListener(new CancelClik());
    		
    		buildTabs(pane);
    		
    		add(pane);
    		
    		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    		setClosable(true);

        	pack();
        	setVisible(true);
        	setName(name);
        	setTitle(CommonsI18n.tr("Application configuration"));
        	getRootPane().setDefaultButton(okay);
    		MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
        	setLocation(MainGui.centerOnDesktop(getSize()));
        	try {
    			setSelected(true);
    		} catch (PropertyVetoException e) {}			

    	}

    	/**
    	 * Modification actions on the configuration window
    	 * @param e
    	 */
        public void actionPerformed(ActionEvent e) {
        	modified = true;
        }
    	
    	/**
    	 * Initialize various <code>JLabels</code>
    	 * 
    	 * @param pane <code>FormPanel</code> to use to get labels from the
    	 * abeille form
    	 */
    	private void initLabels(FormPanel pane){
    		serveur_label = pane.getLabel("serveur_label");
    		serveur_label.setText(CommonsI18n.tr("SQL server (IP or hostname)"));
    		serveur_label.setToolTipText(CommonsI18n.tr("Enter IP address or host name for the database server"));
    		
			db_label = pane.getLabel("base_label");
			db_label.setText(CommonsI18n.tr("Database"));
			db_label.setToolTipText(CommonsI18n.tr("Enter database name"));
			
			db_user_label = pane.getLabel("user_label");
			db_user_label.setText(CommonsI18n.tr("Database user name"));
			db_user_label.setToolTipText(CommonsI18n.tr("Enter database user name"));
			
			db_passe_label = pane.getLabel("user_passe_label");
			db_passe_label.setText(CommonsI18n.tr("Database user password"));
			db_passe_label.setToolTipText(CommonsI18n.tr("Enter database user password"));
			
			passe_label = pane.getLabel("appli_passe_label");
			passe_label.setText(CommonsI18n.tr("Application password"));
			passe_label.setToolTipText(CommonsI18n.tr("Enter main application password (not implemented)"));
			
			crypt_label = pane.getLabel("cryptage_label");
			crypt_label.setText(CommonsI18n.tr("Passwords encryption mode:"));
			crypt_label.setToolTipText(CommonsI18n.tr("Select encryption method to store passwords in the XML file"));
			
			general_label = pane.getLabel("general_label");
			general_label.setText(CommonsI18n.tr("Main"));
			
			datas_label = pane.getLabel("datas_label");
			datas_label.setText(CommonsI18n.tr("Data access"));    		
    	}
    	
    	/**
    	 * Builds a <code>JTabbedPane</code> according to SQL
    	 * providers.
    	 * 
    	 * @param pane <code>FormPanel</code> to use to get the
    	 * <code>JTabbedPane</code> from the abeille form
    	 */
    	private void buildTabs(FormPanel pane){
    		tabbedPane = pane.getTabbedPane("tabs");
    		
    		Iterator<?> i = providers.iterator();
    		int count = 0;
    		int dimTablo = providers.size();
    		
    		providerMode = new JComboBox[dimTablo];
    		provider = new JTextField[dimTablo];
    		providerName = new JTextField[dimTablo];
    		providerPort = new JTextField[dimTablo];
    		providerDefault = new JRadioButton[dimTablo];
    		providerAttributes = new JTextField[dimTablo];
    		
        	ButtonGroup group = new ButtonGroup();

        	while(i.hasNext()){
        		FormPanel tab = new FormPanel( "be/xtnd/commons/gui/descriptions/database_option_tab.jfrm" );

        		JLabel modeLabel = tab.getLabel("mode_label");
        		modeLabel.setText(CommonsI18n.tr("Server mode"));
        		modeLabel.setToolTipText(CommonsI18n.tr("Select server mode"));
        		
            	JLabel providerLabel = tab.getLabel("base_provider_label");
            	providerLabel.setText(CommonsI18n.tr("Provider"));
            	providerLabel.setToolTipText(CommonsI18n.tr("Enter database provider class name"));

            	JLabel providerNameLabel = tab.getLabel("base_nom_label");
            	providerNameLabel.setText(CommonsI18n.tr("Name"));
            	providerNameLabel.setToolTipText(CommonsI18n.tr("Enter provider name"));

            	JLabel providerPortLabel = tab.getLabel("base_port_label");
            	providerPortLabel.setText(CommonsI18n.tr("Port"));
            	providerPortLabel.setToolTipText(CommonsI18n.tr("Enter database server port"));

            	JLabel providerDefaultLabel = tab.getLabel("base_defaut_label");
            	providerDefaultLabel.setText(CommonsI18n.tr("Default?"));
            	providerDefaultLabel.setToolTipText(CommonsI18n.tr("Define current provider as default"));
            	
            	JLabel attributesLabel = tab.getLabel("attributs_label");
            	attributesLabel.setText(CommonsI18n.tr("Optionnal attributes"));
            	attributesLabel.setToolTipText(CommonsI18n.tr("Enter here full connection attributes string (including separators)"));

        	    Element courant = (Element)i.next();
            	tabbedPane.addTab(courant.getAttributeValue("name"),tab);

            	providerMode[count] = tab.getComboBox("mode");
            	providerMode[count].addItem("server");
            	providerMode[count].addItem("file");
            	providerMode[count].setSelectedItem(courant.getAttributeValue("mode"));
            	
            	provider[count] = tab.getTextField("base_provider");
            	configFields(provider[count], courant.getText());
            	
        		providerName[count] = tab.getTextField("base_nom");
            	configFields(providerName[count], courant.getAttributeValue("name"));
            	
            	providerPort[count] = tab.getTextField("base_port");
            	configFields(providerPort[count], courant.getAttributeValue("port"));
            	
            	providerAttributes[count] = tab.getTextField("attributs");
            	configFields(providerAttributes[count], courant.getAttributeValue("arguments"));
            	
            	providerDefault[count] = tab.getRadioButton("base_defaut");
            	providerDefault[count].addActionListener(this);
                group.add(providerDefault[count]);
            	
                if(courant.getAttribute("default").getValue().equals("yes")){
            		providerDefault[count].setSelected(true);
            		tabbedPane.setSelectedIndex(tabbedPane.getComponentCount()-1);
            	}            	
            	count++;
        	}
    		
    	}
    	
    	/**
    	 * Configures every <code>JTextField</code> and associate them 
    	 * a <code>KeyListener</code>.
         * 
    	 * @param field <code>JTextField</code> to change
    	 * @param text text to display
    	 */
    	private void configFields(JTextField field, String text){
        	field.setText(text);
        	field.setPreferredSize(fieldsSize);
        	field.addKeyListener(new KeyAdapter() {
        		public void keyTyped(KeyEvent e) {
        			modified = true;
    			}
    		});
        }
    	
    	/**
    	 * Configure every <code>JPasswordField</code> and associate them 
    	 * a <code>KeyListener</code>.
         * 
    	 * @param field <code>JPasswordField</code> to change
    	 * @param passe password to display
    	 */
        private void configFields(JPasswordField field, String passe){
        	field.setText(passe);
        	field.setPreferredSize(fieldsSize);
        	field.addKeyListener(new KeyAdapter() {
        		public void keyTyped(KeyEvent e) {
        			modified = true;
    			}
   			});
        }
        
        /* ACTIONS */  
    	/** Define the click event on the "Simple" button */
    	class SimpleClik implements ActionListener {
    		/** 
    		 * Click on "simple" button
    		 * 
    		 *  @param e ActionEvent
    		 */
    		public void actionPerformed(ActionEvent e) {
    			if(MainGui.verifUnicite("cryptowindow",null)){
        			Cryptographie crypto = new Cryptographie();
        			crypto.new CryptoWindow();    				
    			}
    		}
    	}
   
    	/** Define the click event on the "Ok" button */
    	class OkClik implements ActionListener {
    		/**
    		 * Click on the "Ok" button. Displays a confirmation message if data
    		 * have been modified, reloads database and close the window.
    		 * 
    		 * @param e ActionEvent
    		 */
    		public void actionPerformed(ActionEvent e) {
    			if(modified){
    				int response = JOptionPane.showConfirmDialog(
    						MainGui.desktop, 
							CommonsI18n.tr("Save modifications?"),
							CommonsI18n.tr("Configuration"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
    				if (response == JOptionPane.YES_OPTION) {
    					saveConfig(true);
    				}
    			}
    			modified=false;
    			try {parentFrame.loadDb();} catch (Exception e1) {
    				//FIXME error management
    			}
    			dispose();
    		}
    	}

    	/** Define the click event on the "Apply" button */
    	class ApplyClik implements ActionListener {
    		/**
    		 * Click on "Aplpy" button. Saves the changes.
    		 * 
    		 * @param e ActionEvent
    		 */
    		public void actionPerformed(ActionEvent e) {
    			if(modified){saveConfig(false);}
    			modified=false;
    		}
    	}
    	
    	/** Define the click event on the "Cancel" button */
    	class CancelClik implements ActionListener {
    		/**
    		 * Click on "Cancel" button. Close the window without saving anything.
    		 * 
    		 * @param e ActionEvent
    		 */
    		  public void actionPerformed(ActionEvent e) {
    		  	dispose();
    		  }
    	}
    }

	/**
	 * Get the provider name
	 * @return String
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * Get the lang
	 * @return String
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * Get the SQL provider
	 * @return String
	 */
	public String getSql_provider() {
		return sql_provider;
	}

	/**
	 * Get the SQL server
	 * @return String
	 */
	public String getSql_server() {
		return sql_server;
	}

	/**
	 * Get the SQL user
	 * @return String
	 */
	public String getSql_user() {
		return sql_user;
	}

	/**
	 * Get the SQL password.
	 * @return String
	 * 
	 */
	public String getSql_password() {
		return sql_password;
	}

	/**
	 * Get the mode
	 * @return String
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Get database name
	 * @return String
	 */
	public String getSql_db() {
		return sql_db;
	}

	/**
	 * Get server name
	 * @return String
	 */
	public String getServer_name() {
		return server_name;
	}

	/**
	 * Get connection port
	 * @return String
	 */
	public String getPort() {
		return port;
	}

	/**
	 * Get connection arguments
	 * @return String
	 */
	public String getArguments() {
		return arguments;
	}
}
