/*
 * WindowMenu.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               WindowMenu.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

/**
 * Window manu (reorganization, list)
 * 
 * @author Johan Cwiklinski
 * @since 2005-05-31
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

/**
 * <code>JMenu</code> component that implements "standards" 
 * functionalities for MDI applications.
 * 
 * @see JMenu
 */
public class WindowMenu extends JMenu {
	private static final long serialVersionUID = 8743582773682766512L;
	private MDIDesktopPane desktop;
    private JMenuItem cascade=new JMenuItem(CommonsI18n.trc("Window->Cascade (text)", "Cascade"));
    private JMenuItem tile=new JMenuItem(CommonsI18n.trc("Window->Mosaic (text)", "Mosaic"));
    private JMenuItem close = new JMenuItem (CommonsI18n.trc("Window->Close (text)", "Close"));

    /**
     * Builds window <code>JMenu</code>.
     * 
     * @param desktop <code>JDesktopPane</code> to which windows
     * are attached
     */
    public WindowMenu(final MDIDesktopPane desktop) {
        this.desktop=desktop;
        setText(CommonsI18n.trc("Window menu (text)", "Window"));
        setMnemonic(CommonsI18n.trc("Window menu (mnemonic)", "w").charAt(0));
        cascade.setMnemonic(CommonsI18n.trc("Window->Cascade (mnemonic)", "c").charAt(0));
        cascade.setToolTipText(CommonsI18n.tr("Reorganize windows using cascading mode"));
        cascade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                WindowMenu.this.desktop.cascadeFrames();
            }
        });
        tile.setMnemonic(CommonsI18n.trc("Window->Mosaic (mnemonic)", "m").charAt(0));
        tile.setToolTipText(CommonsI18n.tr("Reorganize windows using mosaic mode"));
        tile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                WindowMenu.this.desktop.tileFrames();
            }
        });
        close.setMnemonic(CommonsI18n.trc("Window->Close (mnemonic)", "o").charAt(0));
        close.setToolTipText(CommonsI18n.tr("Close current window"));
        close.setIcon(MainGui.closeImage);
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JInternalFrame f = desktop.getSelectedFrame();
                if (f != null) {
                      f.dispose();
                }
            }
        });
        
        addMenuListener(new MenuListener() {
            public void menuCanceled (MenuEvent e) {}

            public void menuDeselected (MenuEvent e) {
                removeAll();
            }

            public void menuSelected (MenuEvent e) {
                buildChildMenus();
            }
        });
    }
    
    /**
     * Builds children menus regarding to current DesktopPane state 
     */
    private void buildChildMenus() {
        int i;
        ChildMenuItem menu;
        JInternalFrame[] array = desktop.getAllFrames();

        add(cascade);
        add(tile);
        addSeparator();
        add(close);
        if (array.length > 0) addSeparator();
        cascade.setEnabled(array.length > 0);
        tile.setEnabled(array.length > 0);

        for (i = 0; i < array.length; i++) {
            menu = new ChildMenuItem(array[i]);
            menu.setState(i == 0);
            menu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    JInternalFrame frame = ((ChildMenuItem)ae.getSource()).getFrame();
                    frame.moveToFront();
                    try {
                        frame.setSelected(true);
                    } catch (PropertyVetoException e) {
                        e.printStackTrace();
                    }
                }
            });
            menu.setIcon(array[i].getFrameIcon());
            add(menu);
        }
    }

    /**
     * <code>JCheckBoxMenuItem</code> with each open window
     */
    class ChildMenuItem extends JCheckBoxMenuItem {
		private static final long serialVersionUID = -3559813875378638729L;
		private JInternalFrame frame;

		/**
		 * 
		 * @param frame JInternalFrame
		 */
        public ChildMenuItem(JInternalFrame frame) {
            super(frame.getTitle());
            this.frame=frame;
        }

        /**
         * 
         * @return JInternalFrame
         */
        public JInternalFrame getFrame() {
            return frame;
        }
    }
}
