/*
 * UtilsChooser.java, 2005-09-07
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               	UtilsChooser.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.utils;

import java.io.File;

/**
 * File utilities
 * 
 * @author Johan Cwiklinski
 * @since 2005-09-07
 * @version 1.0
 */
public class UtilsChooser{
    /* IMAGES */
	public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String tiff = "tiff";
    public final static String tif = "tif";
    public final static String png = "png";
    /* DATABASE FILES */
	/** HSQLDB files extension */
    public final static String hsqldb = "script";
    public final static String bdd = "sql";
    public final static String xml = "xml";

    /**
     * Get a file extension
     * 
     * @param 	f file
     * @return	file's extension
     */
    public static String getExtension(File f){
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 && i < s.length() - 1)
            ext = s.substring(i + 1).toLowerCase();
        return ext;
    }    
    
    /**
     * Get a file name without extension
     * 
     * @param 	f file
     * @return	file name without extension
     */
    public static String removeExtension(File f){
    	String ext = null;
    	String s = f.getName();
    	int i = s.lastIndexOf('.');
    	if(i==-1) ext = s; //cas name without extension
    	if (i > 0 && i < s.length() - 1)
    		ext = s.substring(0, i).toLowerCase();
    	return ext;
    }
}
