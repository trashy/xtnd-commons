/*
 * XMLFilter.java, 2006-04-03
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               XMLFilter.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.utils;

import java.io.File;

import be.xtnd.commons.i18n.CommonsI18n;

/**
 * Filter on XML files
 * 
 * @author Johan Cwiklinski
 * @since 2006-04-03
 * @version 1.0
 */
public class XMLFilter extends javax.swing.filechooser.FileFilter{

	/**
	 * @param file file
	 * @return true if an XML file or a directory, false otherwise
	 */
	@Override
    public boolean accept(File file) {
    	boolean result = false;
        if (file.isDirectory()){
            return true;
        }
        String extension = UtilsChooser.getExtension(file);
        if (extension != null){
        	result = (extension.equals(UtilsChooser.xml))?true:false;
        }
        return result;
    }
    
	/**
	 * @return Filter description
	 */
	@Override
	public String getDescription() {
		return CommonsI18n.tr("XML Files (*.xml)");
    }
}
