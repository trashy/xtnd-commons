/*
 * ImageFilter.java, 2006-03-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :              	ImageFilter.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
package be.xtnd.commons.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Filter on image files
 * 
 * @author Johan Cwiklinski
 * @since 2006-03-31
 * @version 1.0
 */
public class ImageFilter extends FileFilter{

	/**
	 * @param f file
	 * @return true if file is an image or a directory, false otherwise
	 */
    public boolean accept(File f){
        if (f.isDirectory()){
            return true;
        }

        String extension = UtilsChooser.getExtension(f);
        if (extension != null){
            if (extension.equals(UtilsChooser.gif) || 
                extension.equals(UtilsChooser.jpeg) || 
                extension.equals(UtilsChooser.jpg)){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    /**
     * @return Filter description
     */
    public String getDescription(){
        return CommonsI18n.tr("Image files (*.gif, *.jpg, *.jpeg)");
    }
}
