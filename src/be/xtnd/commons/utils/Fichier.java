/*
 * Fichier.java, 2005-06-17
 * 
 * This file is part of X-TnD Commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski, Thierry Selva
 *
 * File :               Fichier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * File utilities
 * 
 * @author Thierry Selva
 * @since 2005-06-17
 */
public class Fichier {
    private BufferedReader fR;
    private BufferedWriter fW;
    private char mode;
    
    /**
     * 
     * @param nomDuFichier
     * @param s
     * @throws IOException
     */
    public void ouvrir(String nomDuFichier, String s) throws IOException {
        mode=(s.toUpperCase()).charAt(0);
        if (mode=='R' || mode=='L') fR=new BufferedReader(new InputStreamReader(new FileInputStream(nomDuFichier), "ISO-8859-1"));
        else if (mode=='W' || mode=='E') fW=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomDuFichier), "ISO-8859-1"));
    }
    
    /**
     * 
     * @param chaine
     * @throws IOException
     */
    public void ecrire(String chaine) throws IOException {
        if (chaine!=null) {
            fW.write(chaine);
            fW.newLine();
        }
    }
    
    /**
     * 
     * @return String
     * @throws IOException
     */
    public String lire() throws IOException {
        String chaine=fR.readLine();
        return chaine;
    }
    
    /**
     * 
     * @throws IOException
     */
    public void fermer() throws IOException {
        if (mode=='R' || mode=='L') fR.close();
        else if (mode=='W' || mode=='E') fW.close();
    }
}
