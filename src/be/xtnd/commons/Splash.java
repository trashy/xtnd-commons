/*
 * Splash.java, 2005-06-12
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Splash.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UIManager;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;

 /**
  * Displays a splash screen with progress bar
  *
  * @author Johan Cwiklinski
  * @since 2005-06-12
  * @version 0.8
  */
public class Splash extends JWindow implements Runnable {
	private static final long serialVersionUID = -7448789973486318253L;
	private JProgressBar progressBar;
    private SimulatedActivity activity;
    private Timer activityMonitor;
    private int delay;
    private Thread thread;
    /** Defines is splash has reached its end */
    public boolean ended = false;
    private ResourceBundle bundle;
	static Logger logger = Logger.getLogger(Splash.class.getName());
   
    /**
     * Default constructor. Used for graphical components.
     * 
     * @see #adjustFont()
     */
    public Splash(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
    	adjustFont();
    }
    
    /**
     * Displays a splash screen for adelimited duration. initalize
     * graphical components. 
     * 
     * @param d display duration
     * @param bundle ResourceBundle
     * 
     * @see #adjustFont()
     */
    public Splash(int d, ResourceBundle bundle){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
    	thread = new Thread(this);
    	thread.setName("splash");
    	this.delay = d;
    	this.bundle = bundle;
    	buildSplash();
    }
    
    /** Starts thread */
    public void start(){
    	thread.start();
    }
   
    /** Buildw window */
    public void buildSplash() {
    	adjustFont();
    	progressBar = new JProgressBar();
    	progressBar.setStringPainted(true);
    	progressBar.setMaximum(delay);
    	activity = new SimulatedActivity(delay);
    
    	JPanel p = new JPanel();
    	p.setLayout(new BorderLayout());
	
    	FormPanel pane = new FormPanel( "be/xtnd/commons/gui/descriptions/header.jfrm" );

    	GuiCommons.createHeader(pane, "biglogo", bundle);

    	p.add(pane, BorderLayout.CENTER);

    	JPanel south = new JPanel();
    	south.setLayout(new GridLayout(2,0));

    	JLabel label = new JLabel(CommonsI18n.tr("Loading in progress..."));
    	label.setHorizontalAlignment(SwingConstants.CENTER);

    	south.add(label);
    	south.add(progressBar);
    	p.add(south, BorderLayout.SOUTH);
    
    	getContentPane().add(p);
    	pack();
    	setLocationRelativeTo(getParent());
    }
  
    /** Starts thread */
    public void run() {
    	setVisible(true);
    	activity.start();

    	activityMonitor = new Timer(100, new ActionListener(){ 
    		public void actionPerformed(ActionEvent event){ 
    			int current = activity.getCurrent();
    			progressBar.setValue(current);

    			if (current == activity.getTarget()){ 
    				activityMonitor.stop();
    				activity.interrupt();
    				ended=true;
    			}
    		}
    	});
    	activityMonitor.start();   
    }
    
    /** Ends thread */
    public void end(){
    	try {
    		Thread.sleep(500);
    		activity.interrupt();
    		activityMonitor=null;
    		ended=true;
    		dispose();
    	} catch (InterruptedException e) {
    		logger.error("InteruptedException from Splash : "+e.getMessage());
    		e.printStackTrace();
    	}
    }

    /**
     * Displays splash screen for delimited duration (in milliseconds)
     * and then hide it.
     * 
     * @param delay display duration
     */
    public void dispose(int delay) {
    	dispose();
    	Splash s = new Splash(delay, bundle);
    	s.dispose();
    }
 
    /** Defines main application appearance */
    public void adjustFont(){
    	PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
  	
    	try {
    		UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
    		UIManager.put("jgoodies.popupDropShadowEnabled", Boolean.TRUE);
    	} catch (Exception e) {
    		logger.warn("Exception initializing L&F : "+e.getMessage());
    	}
    }
    
	/**
	 * Simulate activity. Would be replaced with a real activity detection...
	 */
	class SimulatedActivity extends Thread{
	  /**
	     Constructs the simulated activity thread object. The
	     thread increments a counter from 0 to a given target.
	     @param t the target value of the counter.
	  */
	  public SimulatedActivity(int t){ 
	     current = 0;
	     target = t;
	  }
	
	  /**
	   * 
	   * @return int
	   */
	  public int getTarget(){ 
	     return target;
	  }
	
	  /**
	   * 
	   * @return int
	   */
	  public int getCurrent(){ 
	     return current;
	  }
	
	  /**
	   * 
	   */
	  public void run(){ 
	     try{
	        while (current < target && !interrupted()){   
	           sleep(500);
	           current++;
	        }
	     }
	     catch(InterruptedException e){}
	  }
	
	  private int current;
	  private int target;
	} 
}