/*
 * Cryptographie.java, 2005-08-29
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Cryptographie.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.security;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.components.JSpinField;

/**
 * Encrypt and/or decrypt a string.
 * Also get possibility to generate random password.
 * <code>CryptoWindow</code> gives a basic GUI
 *
 * @author Thierry Selva
 * @author Johan Cwiklinski
 * @since 2005-08-29
 * @version 1.0
 */
public class Cryptographie {
    private static int base=47,intervalle=109-base;
	
    private static int modifie_intervalle(int n,int sens) {
	    if (sens==-1) {
	        if (n>96) return n-13;
	        else if (n>64) return n-7;
	        else return n;
	    } else {
	        if (n>83) return n+13;
	        else if (n>57) return n+7;
	        else return n;
	    }
	}
    
    /**
     * Generate a specified lenght string
     * 
     * @param n lenght
     * @return random string
     */
    public static String aleatoire(int n) {
        Random generateur = new Random(System.currentTimeMillis());
        int r;
        StringBuffer chaine=new StringBuffer(n);
        for (int i=0;i<n;i++) {
            r=generateur.nextInt(intervalle);
            chaine.append((char) modifie_intervalle(r+base+1,1));
        }
        return chaine.toString();
    }
    
    private static String cryptage(String password,int position) {
	    int longueur=password.length()/*,m*/;
	    StringBuffer crypte=new StringBuffer(longueur);
	    int init=modifie_intervalle((int) password.charAt(position),-1)-base;
	    //char c;
	    for (int i=0;i<longueur;i++) {
	        if (i==position) crypte.append(password.charAt(position));
	        else crypte.append((char) modifie_intervalle((modifie_intervalle((int) password.charAt(i),-1)+init-(base+1))%intervalle+(base+1),1));
	        init=modifie_intervalle((int) password.charAt(i),-1)-base;
	    }
	    return crypte.toString();
	}
	
	private static String decryptage(String crypte,int position) {
	    int base=47,intervalle=109-base;
	    int longueur=crypte.length();
	    StringBuffer password=new StringBuffer(longueur);
	    int n,init=modifie_intervalle((int) crypte.charAt(position),-1)-base;

	    for (int i=0;i<longueur;i++) {
	        if (i==position) password.append(crypte.charAt(position));
	        else {
	            n=modifie_intervalle((int) crypte.charAt(i),-1)-init-base;
	            n=(n<1)?n+=intervalle:n;
	            password.append((char) modifie_intervalle(n+base,1));
	        }
	        init=modifie_intervalle((int) password.charAt(i),-1)-base;
	    }
	    return password.toString();
	}
	
	/**
	 * Encrypt a string
	 * 
	 * @param chaine string to encrypt
	 * @return encrypted string
	 */
	public static String crypte(String chaine) {
	    if(chaine.equalsIgnoreCase("")){
	    	return null;
	    }else{
	    	return cryptage(cryptage(chaine,chaine.length()-2),2);	    	
	    }
	}
	
	/**
	 * Decrypt a string
	 * 
	 * @param chaine encrypted string
	 * @return decrypted string
	 */
	public static String decrypte(String chaine) {
	    if(chaine.equalsIgnoreCase("")){
	    	return null;
	    }else{
	    	return decryptage(decryptage(chaine,2),chaine.length()-2);
	    }
	}

	/**
	 * Small basic GUI 
	 * 
	 * @author Johan Cwiklinski
	 * @author Thierry Selva
	 */
	public class CryptoWindow extends EscapeInternalFrame{
		private static final long serialVersionUID = 1030755448571865099L;
		private JButton okay, cancel;
		private TitledSeparator title;
		private TitledBorderLabel mode;
		private JLabel param_label, result_label;
		private JTextField param, result;
		private JRadioButton crypt, decrypt;
		private JCheckBox aleat;
		private JSpinField nbre_aleat;
		
		/**
		 * Default constructor.
		 * Creates main window and initializes components.
		 * GUI is a simple <code>JInternalFrame</code>, so you'll
		 * have to implement a <code>JFrame</code> and an <code>JDesktopPane</code> 
		 * to put it into.
		 */
		public CryptoWindow(){
    		super();
    		super.setTitle(CommonsI18n.tr("Simple encrypt/decrypt GUI"));
            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
    		FormPanel pane = new FormPanel( "be/xtnd/commons/gui/descriptions/cryptform.jfrm" );

    		initLabels(pane);
    		
    		title = (TitledSeparator)pane.getComponentByName("title");
    		title.setText(CommonsI18n.tr("Simple encrypt/decrypt GUI"));
    		
    		aleat = pane.getCheckBox("aleatoire");
    		aleat.addActionListener(new AleatEvent());
    		aleat.setText(CommonsI18n.tr("Random"));
    		aleat.setToolTipText(CommonsI18n.tr("Generates a random string"));
    		nbre_aleat = (JSpinField)pane.getComponentByName("nbre_aleat");
    		
    		crypt = pane.getRadioButton("crypt");
    		crypt.setText(CommonsI18n.tr("Encrypt"));
    		crypt.setToolTipText(CommonsI18n.tr("Encrypt the specified string"));
    		
    		decrypt = pane.getRadioButton("decrypt");
    		decrypt.setText(CommonsI18n.tr("Decrypt"));
    		decrypt.setToolTipText(CommonsI18n.tr("Decrypt the specified string"));
    		
    		param = pane.getTextField("param");
    		result = pane.getTextField("result");
    		
    		mode = (TitledBorderLabel)pane.getComponentByName("mode");
    		mode.setText(CommonsI18n.tr("Modes"));
    		
    		okay = (JButton)pane.getButton("okay");
    		okay.setText(CommonsI18n.tr("Okay"));
    		okay.addActionListener(new OkayEvent());
    		
    		cancel = (JButton)pane.getButton("annuler");
    		cancel.setText(CommonsI18n.tr("Cancel"));
    		cancel.addActionListener(new CancelEvent());
    		
    		add(pane);

    		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    		setClosable(true);
    		
        	pack();
        	setVisible(true);
        	setName("cryptowindow");
        	getRootPane().setDefaultButton(okay);
    		MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
        	setLocation(MainGui.centerOnDesktop(getSize()));
        	try {
    			setSelected(true);
    		} catch (PropertyVetoException e) {}			

		}
				
		private void initLabels(FormPanel pane){
			param_label = pane.getLabel("param_label");
			param_label.setText(CommonsI18n.tr("String to encrypt/decrypt"));
			
			result_label = pane.getLabel("result_label");
			result_label.setText(CommonsI18n.tr("Encrypted/Decrypted string"));
		}
		
		class OkayEvent implements ActionListener{
			/**
			 * Event fired 
			 * @param e 
			 */
			public void actionPerformed(ActionEvent e) {
				String parametre;
				if(param.getText().equals("") && !aleat.isSelected()) {
	    			JOptionPane.showMessageDialog(
	    					MainGui.desktop, 
	    					CommonsI18n.tr("Be sure to enter a string to encrypt/decrypt"),
							CommonsI18n.tr("Missing parameter"),
							JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
				}else{
					parametre = (aleat.isSelected())?aleatoire(nbre_aleat.getValue()):param.getText();
					if(aleat.isSelected()) param.setText(parametre);
					result.setText((crypt.isSelected())?crypte(parametre):decrypte(parametre));
					result.setText((decrypt.isSelected())?decrypte(parametre):crypte(parametre));
				}
			}		
		}
		class CancelEvent implements ActionListener{
			/**
			 * Event fired
			 * @param e ActionEvent 
			 */
			public void actionPerformed(ActionEvent e) {
				dispose();
			}		
		}

		class AleatEvent implements ActionListener{
			/**
			 * Event fired 
			 * @param e ActionEvent
			 */
			public void actionPerformed(ActionEvent e) {
				nbre_aleat.setEnabled(aleat.isSelected());
			}		
		}
	}
}
