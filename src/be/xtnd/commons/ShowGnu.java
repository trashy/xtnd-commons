/*
 * ShowGnu.java, 2005-07-07
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               ShowGnu.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

/**
 * Shows interactive licence message
 *
 * @author Johan Cwiklinski
 * @since 2005-07-07
 */
public class ShowGnu {
	
	/**
	 * Shows copyright in interactive mode
	 * 
	 * @param app application title
	 * @param year application month and year 
	 * @param author application author
	 */
	public ShowGnu(String app, String year, String author){
    	System.out.println(app + ", Copyright (C) " + year + " " + author);
    	System.out.println("It comes with ABSOLUTELY NO WARRANTY.");
    	System.out.println("This is free software, and you are welcome");
    	System.out.println("to redistribute it under certain conditions,");
    	System.out.println("see license.txt file for details.");
    	System.out.println("*********************************************************");
	}
}
