/*
 * MessagesCommons.java, 2005-08-21
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MessagesCommons.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.i18n.CommonsI18n;

/**
 * Lods content from .properties file. Dedicated to 
 * <code>de be.xtnd.commons.*</code> packages.
 * 
 * @author Johan Cwiklinski
 * @since 2005-08-21
 * @deprecated
 */
public class MessagesCommons {
	private static final String BUNDLE_NAME = "be.xtnd.commons.gui.messages";

	@Deprecated
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	static Logger logger = Logger.getLogger(MessagesCommons.class.getName());

	/**
	 * Search and returns key value.
	 * 
	 * @param key key name
	 * @return key value in property file
	 * @deprecated use {@link CommonsI18n} for i18n 
	 * and {@link ApplicationProperties} for real app properties 
	 */
	public static String getString(String key) {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			logger.warn("missing key : "+key+" - in file : "+BUNDLE_NAME);
			return '!' + key + '!';
		}
	}
}
