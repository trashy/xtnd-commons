/*
 * ProprietesSysteme.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               ProprietesSysteme.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;

import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Property system display
 * 
 * @author Johan Cwiklinski
 * @since 2005-05-31
 */
public class ProprietesSysteme extends EscapeInternalFrame{
	private static final long serialVersionUID = -351189332798036335L;
	private JButton ok;
    private JPanel panelOk;
    private JPanel panel;
    private JScrollPane scrollpane;
    private JTable tableau;

    /**
     * Default constructor
     */
    public ProprietesSysteme(){
        super();
    	this.setResizable(false);
    	this.setIconifiable(false);
    	this.setClosable(true);
    	this.setResizable(true);
    	this.setMaximizable(true);
    	this.setVisible(true);
    	this.setName("system");
    	this.setTitle(CommonsI18n.tr("System properties"));
    	this.pack();
    	this.getRootPane().setDefaultButton(ok);
		MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
		this.toFront();
		try {
			this.setSelected(true);
		} catch (PropertyVetoException e) {e.printStackTrace();}    	
        initComponents();
    }

    private void initComponents()
    {       
        // System Properties table

        // Create the table using the appropriate table model
        SystemPropertiesTableModel spModel = new SystemPropertiesTableModel();
        spModel.load();

        tableau = new JTable(spModel);

        tableau.setRowMargin(0);
        tableau.getColumnModel().setColumnMargin(0);

        TableSorter sorter = new TableSorter(tableau.getModel());
        tableau.setModel(sorter);
        sorter.setTableHeader(tableau.getTableHeader());
        tableau.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

        // Add custom renderers for the table cells and headers
        int tWidth = 30; // arbitrary # of pixels for vertical scrollbar
        for (int iCnt=0; iCnt < tableau.getColumnCount(); iCnt++)
        {
            TableColumn column =
                tableau.getColumnModel().getColumn(iCnt);

            if (iCnt == 0)
            {
                int w = 210;
                column.setPreferredWidth(w); // Property Name
                tWidth += w;
            }
            else
            {
                int w = 320;
                column.setPreferredWidth(w); // Property Value
                tWidth += w;
            }

            column.setHeaderRenderer(new SystemPropertiesTableHeadRend());
            column.setCellRenderer(new SystemPropertiesTableCellRend());
        }

        // Put the table into a scroll panew
        scrollpane = new JScrollPane(
            tableau,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.getViewport().setBackground(
            tableau.getBackground());

        // Put the scroll pane into a panel
        panel = new JPanel(new BorderLayout(10, 10));
        panel.setPreferredSize(new Dimension(tWidth, 300));
        panel.add(
            scrollpane, BorderLayout.CENTER);
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));

        ok = new JButton(CommonsI18n.tr("Ok"));
        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                okPressed();
            }
        });

        panelOk = new JPanel(new FlowLayout(FlowLayout.CENTER));        
        panelOk.add(ok);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(panelOk, BorderLayout.SOUTH);

        getRootPane().setDefaultButton(ok);

        pack();

		int w = (MainGui.desktop.getWidth() - getWidth())/2;
		int h = (MainGui.desktop.getHeight() - getHeight())/2;
		Point location = new Point(w, h);
		setLocation(location);

		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ok.requestFocus();
            }
        });
    }      

    private void okPressed()
    {
        dispose();
    }

}
