/*
 * PrintPreview.java, 2005-06-25
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               PrintPreview.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import be.xtnd.commons.i18n.CommonsI18n;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Defines JasperReports preview window parameters
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-25
 * @version 1.0
 */
public class PrintPreview extends JasperViewer {
	private static final long serialVersionUID = 5145216530724932384L;

	/**
	 * Creates a <code>JasperReports</code> preview window. Title
	 * and icon are pre parameted.
	 * 
	 * @param jasperPrint <code>JasperPrint</code> object to preview
	 * @throws JRException
	 * 
	 * @see JasperViewer
	 */
	public PrintPreview(JasperPrint jasperPrint) throws JRException {
		super(jasperPrint, false);
		super.setTitle(CommonsI18n.tr("Print preview"));
		super.setIconImage(MainGui.icon);
		super.setVisible(true);
	}
}
