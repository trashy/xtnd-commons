/*
 * EscapeJFrame.java, 2005-06-24
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               EscapeJFrame.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 * Extends <code>JFrame</code>, implements exit on echap
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-24
 * @version 1.0
 */
public class EscapeJFrame extends JFrame {
	private static final long serialVersionUID = -6688863192387212183L;

	/**
	 * Defauilt constructor. Creates a <code>JFrame</code>,
	 * attribute it an icon, place it on foreground and fix its size.
	 */
	public EscapeJFrame(){
		super();
		super.setIconImage(MainGui.icon);
		super.setAlwaysOnTop(true);
		super.setResizable(false);
	}
	
	/** FIXME: this code is duplicated in {@link EscapeInternalFrame}
	/**
	 * Creates a popup window on right-click.
	 * 
	 * @param popup <code>JPopupMenu</code> to display
	 * @param text menu text
	 * 
	 */
	protected void popupMenu(JPopupMenu popup, String text){
		JMenu menu = new JMenu(text);
		popup.add(menu);
	}
	
	/** FIXME: this code is duplicated in {@link EscapeInternalFrame}
	/**
	 * Configures popup menu entries
	 * 
	 * @param popup original popup menu
	 * @param text menu's text
	 * @param action actionListener
	 * @param icon menu's icon
	 */
	protected void popupEntries(JPopupMenu popup, String text, String action, ImageIcon icon){
		JMenuItem item = new JMenuItem(text);
		item.setActionCommand(action);
		item.setIcon(icon);
		item.addActionListener(new java.awt.event.ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	popupActions(e);
	            }
	        });
		popup.add(item);
	}	
	
	/** FIXME: this code is duplicated in {@link EscapeInternalFrame}
	/**
	 * Configure popup menu's sub entries.
	 * 
	 * @param popup original popup menu
	 * @param menu parent <code>JMenu</code>
	 * @param text menu's text
	 * @param action actionListener
	 * @param icon menu's icon
	 */
	protected void popupEntries(JPopupMenu popup, JMenu menu, String text, String action, ImageIcon icon){
		JMenuItem item = new JMenuItem(text);
		item.setActionCommand(action);
		item.setIcon(icon);
		item.addActionListener(new java.awt.event.ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	popupActions(e);
	            }
	        });
		menu.add(item);
		popup.add(menu);
	}	

	/** FIXME: this code is duplicated in {@link EscapeInternalFrame}
	/** 
	 * Popups actions
	 * @param event ActionEvent 
	 */
	protected void popupActions(ActionEvent event) {}

	/** FIXME: this code is duplicated in {@link EscapeInternalFrame}
	/**
	 * Creates main container. Cause escape key to close window. 
	 * 
	 * @return JRootPane main panel
	 */
	protected JRootPane createRootPane() {
		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				dispose();
			}
		};
		JRootPane rootPane = new JRootPane();
		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.registerKeyboardAction(actionListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		return rootPane;
	}
	
	
}

