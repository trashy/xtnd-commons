/*
 * SystemPropertiesTableCellRend.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               SystemPropertiesTableCellRend.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Custom cell renderer for the cells of the System Properties table
 * of ProprietesSysteme.
 * 
 * @author Johan Cwiklinski
 * @since 2005-05-31
 */
class SystemPropertiesTableCellRend extends DefaultTableCellRenderer
{
	private static final long serialVersionUID = -3605840466006032148L;

	/**
     * Returns the rendered cell for the supplied value and column.
     *
     * @param jtSystemProperties The JTable
     * @param value The value to assign to the cell
     * @param bIsSelected True if cell is selected
     * @param iRow The row of the cell to render
     * @param iCol The column of the cell to render
     * @param bHasFocus If true, render cell appropriately
     * @return The renderered cell
     */
    public Component getTableCellRendererComponent(
        JTable jtSystemProperties, Object value, boolean bIsSelected,
        boolean bHasFocus, int iRow, int iCol)
    {
        JLabel cell = (JLabel) super.getTableCellRendererComponent(
            jtSystemProperties, value, bIsSelected, bHasFocus, iRow, iCol);
        cell.setHorizontalAlignment(LEFT);       

        cell.setBorder(new EmptyBorder(0, 5, 0, 5));

        return cell;
    }
}
