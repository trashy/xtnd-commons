/*
 * AboutBox.java, 2005-06-04
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               AboutBox.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.WindowConstants;

import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;


/**
 * About window
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-04
 * @version 1.1
 */
public class AboutBox  extends EscapeInternalFrame{
	private static final long serialVersionUID = 2839336320934239900L;
	/** Window name */
	public final static String NAME = "about";
	/** Window position */
	public static Point location;
	private JLabel auteur, auteur_label, date, date_label, ecran, ecran_label, systeme, systeme_label,
		java, java_label, vendeur, vendeur_label, locale, locale_label;
	private JButton bouton_ok, bouton_systeme;
	
	/**
	 * Default constructor. Creates a <code>JInternalFrame</code>.
	 * 
	 * @param bundle ResourceBundle à lire 
	 */
	public AboutBox(ResourceBundle bundle){
		super();
		setName(NAME);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		Object[] args = {bundle.getString("appli.name")};
		setTitle(
				CommonsI18n.tr(
						"About {0}...",
						args
				)
		);
		setResizable(false);
		
		FormPanel pane = new FormPanel("be/xtnd/commons/gui/descriptions/about.jfrm");

		GuiCommons.createHeader(pane, "medlogo", bundle);
		
		auteur = pane.getLabel("auteur");
		auteur.setText(bundle.getString("appli.author"));
		
		auteur_label = pane.getLabel("auteur_label");
		auteur_label.setText(CommonsI18n.tr("Author:"));
		
		date = pane.getLabel("date");
		date.setText(bundle.getString("appli.date"));
		
		date_label = pane.getLabel("date_label");
		date_label.setText(CommonsI18n.tr("Date:"));
		
		ecran = pane.getLabel("ecran");
		Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
		int scrHauteur = (int)tailleEcran.getHeight();
		int scrLargeur = (int)tailleEcran.getWidth();
		ecran.setText(scrLargeur+"x"+scrHauteur+" pixels");
		
		ecran_label = pane.getLabel("ecran_label");
		ecran_label.setText(CommonsI18n.tr("Screen resolution:"));
		
		systeme = pane.getLabel("systeme");
		String osname = System.getProperty ( "os.name" );
		String osversion = System.getProperty ( "os.version" );
		String osarch = System.getProperty( "os.arch" );
		systeme.setText(osname+" - "+osversion+"("+osarch+")");
		
		systeme_label = pane.getLabel("systeme_label");
		systeme_label.setText(CommonsI18n.tr("System:"));
		
		java = pane.getLabel("java");
		String javaversion = System.getProperty( "java.version" );
		String javahome = System.getProperty( "java.home" );
		java.setText(javaversion+" ("+javahome+")");
		
		java_label = pane.getLabel("java_label");
		java_label.setText(CommonsI18n.tr("Java:"));
		
		vendeur = pane.getLabel("vendeur");
		String javavendor = System.getProperty( "java.vendor" );
		String javaurl = System.getProperty( "java.vendor.url" );
		vendeur.setText(javavendor+"("+javaurl+")");
		
		vendeur_label = pane.getLabel("vendeur_label");
		vendeur_label.setText(CommonsI18n.tr("vendor:"));
		
		locale = pane.getLabel("locale");
		String lang = Locale.getDefault().getDisplayName();
		locale.setText(lang);
		
		locale_label = pane.getLabel("locale_label");
		locale_label.setText(CommonsI18n.tr("Locale:"));
		
		bouton_ok = (JButton)pane.getButton("bouton_ok");
		bouton_ok.setText(CommonsI18n.tr("Ok"));
		bouton_ok.addActionListener(new ActionListener(){
	            public void actionPerformed(ActionEvent e) {
	                dispose();
	            }
		});
		
		bouton_systeme = (JButton)pane.getButton("bouton_systeme");
		bouton_systeme.setText(CommonsI18n.tr("System..."));
		bouton_systeme.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	new ProprietesSysteme();
            }
        });
		
		add(pane);

		getRootPane().setDefaultButton(bouton_ok);
		pack();
		setVisible(true);

		setLocation(MainGui.centerOnDesktop(getSize()));
		MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {}
	}	
}
