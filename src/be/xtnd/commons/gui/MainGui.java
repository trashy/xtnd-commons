/*
 * MainGui.java, 2005-06-26
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MainGui.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import be.xtnd.commons.Config;
import be.xtnd.commons.MDIDesktopPane;
import be.xtnd.commons.Splash;
import be.xtnd.commons.WindowMenu;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Application main window
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-026
 * @version 1.1
 */
public abstract class MainGui extends JFrame {
	private static final long serialVersionUID = 1L;
	/** Application icon, image format */
	public static Image icon;
	/** Application icon, ImageIcon format */
	public static ImageIcon imgIcon;
	/** Close image */
	public static ImageIcon closeImage = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/icons/close.png"));
	/** MDI Desktop */
	public static MDIDesktopPane desktop;	
	private Dimension appliSize = new Dimension(800, 600);
    private static JScrollPane scrollPane;
    /**
     * Database connection 
     * @see Database 
     */
    public static Database db;
    /** 
     * Application configuration, populated at launch
     * @see Config 
     */
    public static Config conf;
    /** 
     * Application SplashScreen 
     * @see Splash 
     */
    public static Splash s = new Splash();
    /** Status bar */
    public static JLabel statusBar;
    private Image fond;
    private boolean fondEnabled;
	private static WaitFrame glassPane;

    /**
     * Create a <code>JFrame</code>
     * 
     * @param title frame title
     * @param icon application icon
     * @param rootMenuBar menu bar
     * @param toolBar tools bar
     * @param config configuration file path
     * @param dim window dimensions
     * @param bundle ResourceBundle resource bundle file
     */
	public MainGui(String title, ImageIcon icon, JMenuBar rootMenuBar, JToolBar toolBar, 
			String config, Dimension dim, ResourceBundle bundle) {
		super(title);
		this.appliSize = dim;
		initWindow(icon, rootMenuBar, toolBar, config, bundle);
	}
	
	/**
     * Create a <code>JFrame</code>
     * 
     * @param title frame title
     * @param icon application icon
     * @param rootMenuBar menu bar
     * @param toolBar tools bar
     * @param config configuration file path
     * @param bundle ResourceBundle resource bundle file
	 */
	public MainGui(String title, ImageIcon icon, JMenuBar rootMenuBar, JToolBar toolBar, 
			String config, ResourceBundle bundle) {
		super(title);
		initWindow(icon, rootMenuBar, toolBar, config, bundle);		
	}

	/**
     * Create a <code>JFrame</code>
     * 
     * @param title frame title
     * @param icon application icon
     * @param rootMenuBar menu bar
     * @param toolBar tools bar
     * @param config configuration file path
	 * @param fond background image
     * @param bundle ResourceBundle resource bundle file
	 */
	public MainGui(String title, ImageIcon icon, JMenuBar rootMenuBar, JToolBar toolBar, 
			String config, Image fond, ResourceBundle bundle) {
		super(title);
		this.fondEnabled = true;
		this.fond = fond;
		initWindow(icon, rootMenuBar, toolBar, config, bundle);
	}

	/**
     * Create a <code>JFrame</code>
     * 
     * @param title frame title
     * @param icon application icon
     * @param rootMenuBar menu bar
     * @param toolBar tools bar
     * @param config configuration file path
     * @param dim window dimensions
	 * @param fond background image
     * @param bundle ResourceBundle resource bundle file
	 */
	public MainGui(String title, ImageIcon icon, JMenuBar rootMenuBar, JToolBar toolBar, 
			String config, Dimension dim, Image fond, ResourceBundle bundle) {
		super(title);
		this.fondEnabled = true;
		this.fond = fond;
		this.appliSize = dim;
		initWindow(icon, rootMenuBar, toolBar, config, bundle);
	}

	/**
	 * Initialize window and load configuration
	 * 
	 * @param icon application icon
	 * @param rootMenuBar menu bar
	 * @param toolBar tools bar
	 * @param config configuration file path
	 * @param bundle ResourceBundle resource bundle file
	 * 
	 * @see Config
	 */
	private void initWindow(ImageIcon icon, JMenuBar rootMenuBar, JToolBar toolBar, String config,
			ResourceBundle bundle){
		s = new Splash(10, bundle);
		glassPane = new WaitFrame(CommonsI18n.tr("Loading in progress..."));

		s.start();
		this.setSize(appliSize);
		this.setLocationRelativeTo(this.getParent());
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		desktop = (this.fondEnabled)?new MDIDesktopPane(fond):new MDIDesktopPane();
		scrollPane = new JScrollPane();
		MainGui.imgIcon = icon;
		MainGui.icon = icon.getImage();
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) { 
				int response = JOptionPane.showConfirmDialog(
						desktop, 
						CommonsI18n.tr("Are you sure you want to exit the application?"),
						CommonsI18n.tr("Quit confirmation"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
	    		if (response == JOptionPane.YES_OPTION) {
	    			try{new Database().disconnect();}catch(Exception ex){/*just in case...*/}
	    			System.exit(0);
	    		}
			}
		    });
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		int menuPosition = 0;
		if(rootMenuBar.getComponentCount()!=0) menuPosition = rootMenuBar.getComponentCount()-1; 
		rootMenuBar.add(new WindowMenu(desktop),menuPosition);
		this.setJMenuBar(rootMenuBar);
	    this.setIconImage(MainGui.icon);

		if(toolBar!=null)
			contentPane.add(toolBar, BorderLayout.NORTH);

		scrollPane.getViewport().add(desktop);
    	contentPane.add(scrollPane,BorderLayout.CENTER);
    	contentPane.add(status(), BorderLayout.SOUTH);
		desktop.getRootPane().setGlassPane(glassPane);

		conf = new Config(config);
		conf.loadConfig(false);

		Object c = new Object();
		while(conf.loaded==false){
			try{
				s.end();
				synchronized(c) { c.wait(500); }
				System.out.print(".");
			} catch(InterruptedException ex) { }			
		}

		Object o=new Object();
		System.out.print(CommonsI18n.tr("Loading in progress..."));
		while(s.ended==false){
			try{
				synchronized(o) { o.wait(500); }
				System.out.print(".");
			} catch(InterruptedException ex) { }
		}
		System.out.println(".");
		o=null;
		this.setVisible(true);
		verifUnicite(Config.name,null);
		s.end();		
		try {loadDb();} catch (Exception e1) {}
	}	
	
	/**
	 * Create a label to display various statuses 
	 * 
	 * @return JLabel statuses label
	 */
	private JLabel status(){
		 statusBar = new JLabel();
		 statusBar.setPreferredSize(new Dimension(200,20));
		 return statusBar;
	}
    
    /**
     * Check window uniqueness on its name.
     * 
     * @param name window name
     * @param defaultLoc window default placement
     * 
     * @return true if window already exists, false otherwise
     */
	public static boolean verifUnicite(String name, Point defaultLoc){
		JInternalFrame[] frames = desktop.getAllFrames();
		boolean load = true;
		int i = 0;
		while(i < frames.length){
			if(frames[i].getName().equals(name)){
				System.out.println("name matches : "+name);
				load = false;
				frames[i].toFront();
				if(defaultLoc != null)
					frames[i].setLocation(defaultLoc);
				try {
					frames[i].setSelected(true);
				} catch (PropertyVetoException e) {}
			}
			i++;
		}
		return load;
	}

	/**
	 * Center an MDI child window on the Desktop
	 * 
	 * @param dim
	 * @return Point window placement
	 */
	public static Point centerOnDesktop(Dimension dim){
		double w = (desktop.getWidth() - dim.getWidth())/2;
		double h = (desktop.getHeight() - dim.getHeight())/2;
		if(w<0) w = 20;
		if(h<0) h = 20;
		return new Point((int)w, (int)h);
	}

	/** FIXME 0.0.2: remove */
	/**
	 * (Dés)Active l'écran d'attente avec le message par défaut
	 * @param start true pour activer l'écran, false pour le désactiver
	 * @deprecated Seems to not be used anymore...
	 */
	public static void attente(boolean start){
		attente(start, "-1");
	}
	
	/**
	 * Activate or deactivate stand by panel.
	 * 
	 * @param start true to activate, false to deactivate
	 * @param msg message to show or '-1' for default message
	 */
	public static void attente(boolean start, String msg){
		if(!msg.equals("-1")) glassPane.setText(msg);
		if(start)
			glassPane.start();
		if(!start)
			glassPane.stop();
	}

	/**
	 * Database loading
	 * 
	 * @throws Exception 
	 */
	public abstract void loadDb() throws Exception;
}
