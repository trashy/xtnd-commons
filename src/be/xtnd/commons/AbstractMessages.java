/*
 * AbstractMessages.java, 2005-10-19
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               	AbstractMessages.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.i18n.CommonsI18n;

/**
 * Usage :
 * <code>
 * public class Messages extends AbstractMessages {
 *     private static final String BUNDLE_NAME = "path-to-messages";
 *  
 *     public static String getMsg(String key){
 *         return getString(key, BUNDLE_NAME);
 *     }
 * }
 * </code>
 *
 * @author Johan Cwiklinski
 * @since 2005-10-19
 */
abstract public class AbstractMessages {
	static Logger logger = Logger.getLogger(AbstractMessages.class.getName());

	/**
	 * Search and return a key value
	 * 
	 * @param key key to look for
	 * @param bundle path to resource file
	 * @return key value in the resource file
	 * @deprecated use {@link ApplicationProperties} instead to get real
	 * properties and {@link CommonsI18n} (or {Project}I18n for i18n
	 */
	protected static String getString(String key, String bundle) {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {
			return getBundle(bundle).getString(key);
		} catch (MissingResourceException e) {
			logger.warn("missing key : "+key+" - in file : "+ bundle);
			return '!' + key + '!';
		}
	}
	
	protected static ResourceBundle getBundle(String name){
		ResourceBundle rb = ResourceBundle.getBundle(name);
		return rb;
	}
}
