/*
 * CommonsI18n.java, 27 mars 2010
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2010 Johan Cwiklinski
 *
 * File :               CommonsI18n.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.i18n;

import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 * @author trasher
 * @since 2010-03-27
 */
public class CommonsI18n {
	private static final String BUNDLE_NAME = "be.xtnd.commons.i18n.Messages";
	public static I18n i18n = I18nFactory.getI18n(CommonsI18n.class, BUNDLE_NAME);

	public static String tr(String text){
		return i18n.tr(text);
	}

	public static String tr(String text, Object[] objects){
		return i18n.tr(text, objects);
	}

	public static String tr(String text, Object o1){
		return i18n.tr(text, o1);
	}

	public static String tr(String text, Object o1, Object o2){
		return i18n.tr(text, o1, o2);
	}

	public static String tr(String text, Object o1, Object o2, Object o3){
		return i18n.tr(text, o1, o2, o3);
	}

	public static String tr(String text, Object o1, Object o2, Object o3, Object o4){
		return i18n.tr(text, o1, o2, o3, o4);
	}

	public static String trc(String context, String text){
		return i18n.trc(context, text);
	}
}
