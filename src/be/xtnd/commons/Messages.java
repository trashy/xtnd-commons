/*
 * Messages.java, 24 juin 2005
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Messages.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/** FIXME 0.0.2: remove */
/**
 * Charge les messages contenu dans le fichier .properties
 * 
 * @author Johan Cwiklinski
 * @deprecated remplacée par une instance de {@link AbstractMessages}
 */
@Deprecated
public class Messages {
	private static final String BUNDLE_NAME = "be.xtnd.gui.messages";//$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	static Logger logger = Logger.getLogger(Messages.class.getName());

	/**
	 * Cherche et retorune la valeur d'une clé
	 * 
	 * @param key clé à chercher
	 * @return valeur de la clé dans le fichier
	 */
	public static String getString(String key) {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			logger.warn("missing key : "+key+" - in file "+ BUNDLE_NAME);
			return '!' + key + '!';
		}
	}
}
