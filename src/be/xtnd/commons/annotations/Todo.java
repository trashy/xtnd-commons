/*
 * Todo.java, 18 oct. 2005
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Todo.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.commons.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Crée des tâches à effectuer
 * Souffre du manque de support des EDI de l'api APT
 * 
 * @author Johan Cwiklinski
 * @since 2005-10-18
 * @see java.lang.annotation.Annotation
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Inherited
public @interface Todo {
	/** Valeur textuelle du todo */
	String value();
	
	/** Niveau de criticité de la tâche. */
	Level  level() default Level.NORMAL;
	
	/** Enumération des différents niveaux de criticités. */
	public static enum Level { 
		/** Priorité basse */
		LOW, 
		/** Priorité normale */
		NORMAL, 
		/** Priorité élevée */
		HIGH, 
		/** Priorité très élevée */
		VERY_HIGH };
	
	/** Programmeur auquel la tâche est attribuée */
	String attributeTo() default "Trasher";
	
	/** Date de réalisaion butoir */
	String endDate() default "";
	
	/** Date de déclaration */
	String writeDate() default "";
}
