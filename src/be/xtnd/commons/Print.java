/*
 * Print.java, 2005-06-26
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Print.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRJdtCompiler;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.PrintPreview;

/**
 * Show <code>JasperReports</code> print window.
 * Original jrxml file must be into <code>be/xtnd/conf</code>.
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-26
 * @version 1.0
 */
public class Print {
	protected String modelUrl;
	protected ResultSet rs;
	//protected boolean withRs = false;
    protected Map<Object, Object> parameters = new HashMap<Object, Object>();
    protected JasperDesign design;
	static Logger logger = Logger.getLogger(Print.class.getName());

	/**
	 * Builds a simple <code>PrintPreview</code>
	 * 
	 * @param url print models path
	 * @param model print model name
	 * 
	 * @see PrintPreview
	 */
	public Print(String url, String model){
		this(url, model, null, null);
	}

    /**
     * Builds a <code>PrintPreview</code> based on a
     * database <code>ResultSet</code>.
     * 
     * @param url print models path
     * @param model print model name
     * @param rs datas <code>ResultSet</code>
     * 
     * @see PrintPreview
     */
	public Print(String url, String model, ResultSet rs){
		this(url, model, rs, null);
	}

	/**
	 * Builds a <code>PrintPreview</code> with parameters 
	 * from a <code>HashMaps</code>.
	 * 
	 * @param url print models path
	 * @param model print model name
	 * @param params parameters <code>HashMaps</code>
	 * 
	 * @see PrintPreview
	 * @see HashMap
	 */
	public Print(String url, String model, HashMap<Object,Object> params){
		this(url, model, null, params);
	}

	/**
	 * Builds a <code>PrintPreview</code> based on a 
	 * database <code>ResultSet</code> and with parameters
	 * from a <code>HashMap</code>.
	 * 
	 * @param url print models path
	 * @param model print model name
	 * @param rs datas <code>ResultSet</code>
	 * @param params parameters <code>HashMaps</code>
	 * 
	 * @see PrintPreview
	 * @see HashMap
	 */
	public Print(String url, String model, ResultSet rs, HashMap<Object,Object> params){
		this.rs = rs;
		this.modelUrl = url+"/"+model;
		this.parameters = params;
		print();	    
	}

	/**
	 * Builds a <code>PrintPreview</code> from a <code>JasperDesign</code>
	 *  
	 * @param design JasperDesign
	 */
	public Print(JasperDesign design){
		this(design, null, null);
	} 

	/**
	 * Builds a <code>PrintPreview</code> from a <code>JasperDesign</code>
	 * object, populated from a <code>ResultSet</code>.
	 * 
	 * @param design JasperDesign
	 * @param rs datas <code>ResultSet</code>
	 */
	public Print(JasperDesign design, ResultSet rs){
		this(design, rs, null);
	} 
	
	/**
	 * Builds a <code>PrintPreview</code> from a <code>JasperDesign</code>
	 * object, parameted with a <code>Hashmap</code>
	 * 
	 * @param design JasperDesign
	 * @param params Paramètres du report
	 */
	public Print(JasperDesign design, HashMap<Object,Object> params){
		this(design, null, params);
	}
	
	/**
	 * Builds a <code>PrintPreview</code> from a <code>JasperDesign</code>
	 * object, populated from a <code>ResultSet</code> and parameted
	 * with a <code>Hashmap</code>.
	 * 
	 * @param design JasperDesign
	 * @param rs datas <code>ResultSet</code>
	 * @param params parameters <code>HashMap</code>
	 */
	public Print(JasperDesign design, ResultSet rs, HashMap<Object,Object> params){
		this.rs = rs;
		this.parameters = params;
		this.design = design;
		print();
	} 

	/**
	 * Generates preview
	 */
	private void print(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {
			JasperDesign jasperDesign;
			jasperDesign = (this.design==null)?JRXmlLoader.load(modelUrl):this.design;
			JasperReport jasperReport = new JRJdtCompiler().compileReport(jasperDesign);
	        
			Database database = (MainGui.db != null)?MainGui.db:new Database(true);			
			Connection connection = database.getConnection();
				        
	        JasperPrint jasperPrint = null;
	        if(this.rs!=null){
	        	JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
	        	jasperPrint = JasperFillManager.fillReport(jasperReport, 
	        			parameters, jrRS);	   
	        }else{
	        	jasperPrint = JasperFillManager.fillReport(jasperReport, 
	        			parameters, connection);
	        }
	        new PrintPreview(jasperPrint);
	        
		} catch (JRException e1) {
			logger.error("JRException when try printing : "+modelUrl+" : "+e1.getMessage());
		}
		
	}
	
	/** FIXME: impossible to copy print models this way, resulting files are corrupted :'( */
	/*private boolean fileExists(String path){
		File f = new File(modelUrl);
		if (!f.exists()) {
			System.out.println("          [Failed]");
    			JOptionPane.showMessageDialog(
    					MainGui.desktop, 
    					CommonsI18n.tr("Print models are missing.\nPlease install them."),
    					CommonsI18n.tr("Missing models"),
						JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
		}
		return f.exists();
	}*/
}
