/*
 * HsqldbErrors.java, 2006-05-25
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               HsqldbErrors.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db.hsql;

import javax.swing.JOptionPane;

import be.xtnd.commons.Config;
import be.xtnd.commons.db.SqlErrors;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

/**
 * Displays some errors from HSQL engine.
 * 
 * @author Johan Cwiklinski
 * @since 2006-05-25
 * @version 1.0
 */
public class HsqldbErrors extends SqlErrors{
	/** Unknow database */
	public static final int UNKNOWN_DATABASE = -94;
	/** Access denied */
	public static final int ACCESS_DENIED = -37;
	/** Database already in use */
	public static final int LAUNCHED_ONCE = -1;
	/** Duplicate */
	public static final int DUPLICATE = -104;
	/** Foreign key failure */
	public static final int FK_FAILS = -8;
	/** Table already exists */
	public static final int TABLE_EXISTS = -27;

	/**
	 * Main constructor
	 */
	public HsqldbErrors(){
		super(
				0,
				UNKNOWN_DATABASE,
				ACCESS_DENIED,
				LAUNCHED_ONCE,
				DUPLICATE,
				FK_FAILS,
				TABLE_EXISTS);
	}
	
	/**
	 * 
	 * @param error
	 * @param par
	 * @param conf
	 */
	public static void showError(int error, MainGui par, Config conf){
		parent = par;
		if (error == LAUNCHED_ONCE){//already running
			JOptionPane.showMessageDialog(MainGui.desktop, 
					CommonsI18n.tr("Application seems to be already running.\nYou cannot execute multiple instances of the application."),
					CommonsI18n.tr("Application already running"),
					JOptionPane.OK_OPTION + JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		if(error == UNKNOWN_DATABASE){//Unknown DB
			Object[] args = {conf.getSql_db(), conf.getServer_name()};
			int response = JOptionPane.showConfirmDialog(
					parent,
					CommonsI18n.tr("<html>Specified database \"<b>{0}</b>\" could not be found on the server \"<b>{1}</b>\".<br><br>Check if it exists and its name was correctly spelled.<br><br>Do you want to go to configuration to fix the problem?</html>", args),
					CommonsI18n.tr("Unable to find database"),
					JOptionPane.ERROR_MESSAGE+JOptionPane.YES_NO_OPTION);		
			if(response==JOptionPane.YES_OPTION){
				conf.new ConfigWindow(par);
			}else{
				cannotContinue();
			}
		}
		if(error == ACCESS_DENIED){//AccessDenied
			Object[] args = {conf.getServer_name(), conf.getSql_db(), conf.getSql_user()};
			int response = JOptionPane.showConfirmDialog(
					parent,
					CommonsI18n.tr("<html>Database connection failed.<br>User or password is probably wrong.<br><br><b><u>Informations:</u></b><br><b>Server:</b> {0}<br><b>Database:</b> {1}<br><b>User:</b> {2}<br><br>Do you want to go to the configuration to fix the problem?</html>", args),
					CommonsI18n.tr("Access denied"),
					JOptionPane.ERROR_MESSAGE+JOptionPane.YES_NO_OPTION);		
			if(response==JOptionPane.YES_OPTION){
				conf.new ConfigWindow(par);
			}else{
				cannotContinue();
			}			
		}
		if(error == TABLE_EXISTS){//table already exists in database
			//TODO show errors
		}
	}
	
    private static void cannotContinue(){
		JOptionPane.showMessageDialog(
				parent,
				CommonsI18n.tr("A fatal error has occured, application cannot continue and is going to be closed."),
				CommonsI18n.tr("Fatal error."),
				JOptionPane.ERROR_MESSAGE+JOptionPane.OK_OPTION);
		System.exit(0);    	
    }
}
