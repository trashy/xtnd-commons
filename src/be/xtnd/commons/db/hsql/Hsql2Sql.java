/*
 * Hsql2Sql.java, 2006-04-05
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               Hsql2Sql.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db.hsql;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.Db2Sql;

/**
 * @author Johan Cwiklinski
 * @since 2006-04-05
 */
public class Hsql2Sql extends Db2Sql {

	/**
	 * 
	 * @param props
	 */
	public Hsql2Sql(Properties props){
		super(props);
	}

	/**
	 * 
	 * @param database
	 */
	public Hsql2Sql(Database database){
		super(database);
	}
	
	/**
	 * 
	 * @param database
	 * @param structure
	 * @param data
	 */
	public Hsql2Sql(Database database, boolean structure, boolean data){
		super(database, structure, data);
	}

	@Override
	protected void getTables(){
    	tables = new ArrayList<String>();
        try {
			DatabaseMetaData dbMetaData = db.getConnection().getMetaData();
            ResultSet rs = dbMetaData.getTables(null, "PUBLIC", null, null);

            while(rs.next()){
            	if(rs.getString("TABLE_TYPE").equalsIgnoreCase("TABLE")){
            		tables.add(rs.getString("TABLE_NAME"));
            	}
            }
		} catch (SQLException e) {
			logger.fatal("SQLException retrieving hsqldb tables");
		}        
	}
	
	@Override
	protected void getCreateTables() {
		String dbPath = null;
		try {
			String dbTmp = db.getSql_server();
			int start = dbTmp.lastIndexOf(":")+1;
			int end = dbTmp.indexOf(";");
			
			dbPath = dbTmp.substring(start, end);
			
			BufferedReader br = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(dbPath+".script"), "ISO-8859-1"));
			String tmp;
        	do {
        		tmp = br.readLine();
        		if(tmp!=null)
        			instructions.add(tmp);
        	} while (tmp!=null);
        	br.close();
		} catch (UnsupportedEncodingException e) {
			logger.fatal("Error saving hsqldb tables : encoding not supported" + e);
		} catch (FileNotFoundException e) {
			logger.fatal("Error saving hsqldb tables : file "+dbPath+" not found" + e);
		} catch (IOException e) {
			logger.fatal("Error saving hsqldb tables : IOException"+e);
		}
	}
}
