/*
 * Database.java, 2005-08-13
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Database.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.gui.MainGui;

/**
 * SQL databases connection, queries execution 
 *
 * @author Johan Cwiklinski
 * @since 2005-08-13
 * @version 1.0
 */
public class Database {
	/** Mode ajout */
	public static final char NEW = 'n';
	/** Mode modification */
	public static final char MODIF = 'm';
	private static Connection connection;
	private Statement statement;
	private ResultSet rs;
	private int rsConcurrency = 0;
	private int rsType = 0;
	private int errCode = 0;
	private String provider_name="", sql_server="", sql_user="", sql_password="", sql_provider="";
	private boolean passwordSet;
	static Logger logger = Logger.getLogger(Database.class.getName());
	
	/**
	 * Define additional connection parameters
	 * 
	 * @param type connection type (see JDBC types)
	 * @param ro read mode (see JDBC parameters)
	 */
	public void setConnectionParameters(int type, int ro){
		this.rsConcurrency = ro;
		this.rsType = type;
	}

	/**
	 * Default constructor
	 * {@link #Database(boolean)}
	 */
	public Database(){
		this(false);
	}
	
	/**
	 * Constructor
	 * 
	 * @param connect Automatic connection or not. If true, connection
	 * will be established by the constructor ; if false, you'll have to
	 * call {@link #connect()} method. It is usefull to pass particular
	 * parameters using {@link #setConnectionParameters(int, int)}.
	 * 
	 * @see #setConnectionParameters(int, int)
	 * @see #connect()
	 */
	public Database(boolean connect){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		if(connect){
			connect();
		}
	}
	
	/**
	 * Create a new Database using provided vairables,
	 * and then calls the {@link #connect()} method. 
	 * 
	 * @param provider_name Name of the provider
	 * @param sql_server    Server's adress
	 * @param sql_user      Database username
	 * @param sql_password  Database user's password
	 * @param sql_provider  SQL provider
	 */
	public Database(String provider_name, String sql_server, String sql_user,
			String sql_password, String sql_provider){
		this(false);
		this.provider_name = provider_name;
		this.sql_server = sql_server;
		this.sql_user = sql_user;
		setSql_password(sql_password);
		this.sql_provider = sql_provider;
		connect();
	}
	
	/**
	 * @return java.sql.Connection object
	 */
	public Connection getConnection() {
		if(connection==null) connect();
		return connection;
	}
	
	/**
	 * Connect to the database, with given parameters (explicitly given to the 
	 * constructor or retrieved by {@link MainGui} 
	 *
	 * @see be.xtnd.commons.Config
	 *
	 * @see #setProvider_name(String)
	 * @see #setSql_password(String)
	 * @see #setSql_provider(String)
	 * @see #setSql_server(String)
	 * @see #setSql_user(String)
	 * or
	 * @see #Database(String, String, String, String, String)
	 */
	public void connect(){
		if(this.provider_name.equals("")) provider_name = MainGui.conf.getProvider_name();
		if(sql_server.equals("")) sql_server = MainGui.conf.getSql_server();
		if(sql_user.equals("")) sql_user = MainGui.conf.getSql_user();
		if(!this.passwordSet) sql_password = MainGui.conf.getSql_password();
		if(sql_provider.equals("")) sql_provider = MainGui.conf.getSql_provider();
		try{
			logger.debug("*** CONNECTION TO DATABASE ***");
			logger.debug("*** PROVIDER : "+this.provider_name);
			logger.debug("*** DATABASE : "+this.sql_server);
			logger.debug("*** DRIVER : "+this.sql_provider);
		    Class.forName(this.sql_provider).newInstance();
		    connection = DriverManager.getConnection(
		    		this.sql_server,
		    		this.sql_user,
		    		this.sql_password
					);
		    if(this.rsConcurrency != 0 && this.rsType != 0){
		    	logger.debug("*** CONNECTION ADDITIONAL PARAMETERS type : "+
		    			this.rsType+", concurrency : "+this.rsConcurrency);
		    	statement = connection.createStatement(this.rsType, this.rsConcurrency);
		    }else{
		    	statement = connection.createStatement();
		    }
		    logger.debug("          [Ok]");
	      }catch ( SQLException E){
	    	  logger.fatal("SQLException while trying to connect database : "+E.getMessage()+" (code "+E.getErrorCode()+" -- sqlstate "+E.getSQLState()+") ");
			  logger.debug("          [Failed]");
	          this.errCode = E.getErrorCode();
	          //unknown host exception from mysql
	          //TODO verify with others drivers if the behavior is the same
			  if(E.getSQLState().equals("08S01") && provider_name.equalsIgnoreCase("mysql")){
				this.errCode = -10;  
			  }
		  }
		  catch ( ClassNotFoundException e){
			  logger.fatal("ClassNotFound while trying to connect database : " + e.getMessage());
			  logger.debug("          [Failed]");
		  }
		  catch ( InstantiationException e){
			  logger.fatal("InstantiationException while trying to connect database : " + e.getMessage());
			  logger.debug("          [Failed]");
		  }
		  catch ( IllegalAccessException e){
			  logger.fatal("IllegalAccessException while trying to connect database : " + e.getMessage());
			  logger.debug("          [Failed]");
		  }
		  catch ( NullPointerException e){
			  logger.fatal("NullPointerException while trying to connect database : " + e.getMessage());
			  logger.debug("          [Failed]");
		  }		  
	}
	
	/**
	 * Disconnect database
	 */
	public void disconnect(){
	  	logger.debug("*** DECONNECTION DATABASE ***");
	  	try {
			connection.close();
		  	logger.debug("          [Ok]");
		} catch (SQLException e) {
			logger.fatal("SQLException while trying to disconnect database : "+e.getMessage()+" (code "+e.getErrorCode()+")");
		  	logger.debug("          [Failed]");
		}
	}
	
	/**
	 * Execute a SELECT SQL query
	 * 
	 * @param strQuery SQL SELECT query to exectue
	 * @return ResultSet
	 * @throws SQLException
	 */
	public ResultSet execQuery(String strQuery) throws SQLException{
	  	logger.debug("*** Execute query "+strQuery+" ***");
        return statement.executeQuery(strQuery);
	}

	/**
	 * Execute a SELECT SQL query on a given statement
	 * 
	 * @param strQuery SQL SELECT query to execute
	 * @param stmt Statement to use
	 * @return ResultSet
	 * @throws SQLException
	 */
	public ResultSet execQuery(String strQuery, Statement stmt) throws SQLException{
	  	logger.debug("*** Execute query "+strQuery+" ***");
        return stmt.executeQuery(strQuery);
	}
	
	/**
	 * Execute a query that give a return value ('UPDATE, INSERT, DELETE, ..)
	 * 
	 * @param strQueryUpdate SQL query to execute
	 * @return affected rows
	 * @throws SQLException
	 */
	public int execUpdate(String strQueryUpdate) throws SQLException{
        logger.debug("*** Execute update "+strQueryUpdate+" ***");
        return statement.executeUpdate(strQueryUpdate);
	}
	  
	/**
	 * Store une requête pour les listes en table
	 * @param query requête à  exécuter
	 */
	/*public void setQuery(String query){
		try {rs = statement.executeQuery(query);}
		catch (SQLException E){
			logger.fatal("SQLException while executing query : "+query+" : "+E.getMessage()+" (code "+E.getErrorCode()+")");
		}
	}*/

	/**
	 * Get ResultSet
	 * @return ResultSet
	 */
	public ResultSet getResultSet(){
		return rs;
	}

	/**
	 * Replace strings
	 * @param s Original string
	 * @param f String to replace
	 * @param r Replacement string
	 * @return Modified string
	 */
	public static String replace( String s, String f, String r ){
	     if (s == null)  return s;
	     if (f == null)  return s;
	     if (r == null)  r = "";

	     int index01 = s.indexOf( f );
	     while (index01 != -1)
	     {
	        s = s.substring(0,index01) + r + s.substring(index01+f.length());
	        index01 += r.length();
	        index01 = s.indexOf( f, index01 );
	     }
	     return s;
	}

	/**
	 * Get the error code
	 * @return Return the last error code 
	 */
	public int getErrCode() {
		return errCode;
	}
	
	/**
	 * Get provider name
	 * @return Return provider name
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * Set provider name
	 * @param provider_name Name of the provider
	 */
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	/**
	 * Get the password
	 * @return SQL user password
	 */
	public boolean hasSql_password() {
		return (sql_password.equals("")) ? false : true;
	}

	/**
	 * Set database user password
	 * @param sql_password The password
	 */
	public void setSql_password(String sql_password) {
		this.passwordSet = true;
		this.sql_password = sql_password;
	}

	/**
	 * Get SQL provider
	 * @return SQL provider class
	 */
	public String getSql_provider() {
		return sql_provider;
	}

	/**
	 * Set SQL provider
	 * @param sql_provider SQL provider class
	 */
	public void setSql_provider(String sql_provider) {
		this.sql_provider = sql_provider;
	}

	/**
	 * Get SQL server
	 * @return SQL server's address
	 */
	public String getSql_server() {
		return sql_server;
	}

	/**
	 * Set SQL server
	 * @param sql_server SQL server's address
	 */
	public void setSql_server(String sql_server) {
		this.sql_server = sql_server;
	}

	/**
	 * Get database username
	 * @return database username
	 */
	public String getSql_user() {
		return sql_user;
	}

	/**
	 * Set database username
	 * @param sql_user username
	 */
	public void setSql_user(String sql_user) {
		this.sql_user = sql_user;
	}
}
