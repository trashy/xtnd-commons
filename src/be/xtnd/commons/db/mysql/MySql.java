/*
 * MySql.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MySql.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import be.xtnd.commons.gui.MainGui;

/** FIXME 0.0.2: remove that class */
/**
 * @author Johan Cwiklinski
 * @since 2005-05-31
 * 
 * @deprecated replaced by more generic be.xtnd.commons.db.Database
 * @see be.xtnd.commons.db.Database
 */
@Deprecated
public class MySql {
	private static Connection connection;
	private Statement statement;
	private ResultSet rs;

	/**
	 * 
	 *
	 */
	public MySql(){connect();}
	
	/**
	 * @return Renvoie connection.
	 */
	public Connection getConnection() {
		return connection;
	}
	
	/**
	 * 
	 *
	 */
	public void connect(){
		try{
		  	System.out.print("*** CONNECTION MYSQL ***");
		    Class.forName(MainGui.conf.getSql_provider()).newInstance();
		    connection = DriverManager.getConnection(
		    		MainGui.conf.getSql_server(),
		    		MainGui.conf.getSql_user(),
		    		MainGui.conf.getSql_password()
					);
		    statement = connection.createStatement();
		    System.out.println("          [Ok]");
	      }catch ( SQLException E){
	      	  System.out.println("          [Failed]");
		  	  System.out.println("SQLException: " + E.getMessage());
	          System.out.println("SQLState:     " + E.getSQLState());
	          System.out.println("VendorError:  " + E.getErrorCode());
		  }
		  catch ( ClassNotFoundException e){System.out.println("          [Failed]");e.printStackTrace();}
		  catch ( InstantiationException e){System.out.println("          [Failed]");System.out.println(e);}
		  catch ( IllegalAccessException e){System.out.println("          [Failed]");System.out.println(e);}
		  catch ( NullPointerException e){System.out.println("          [Failed]");System.out.println(e);}		  
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void disconnect() throws Exception{
	  	System.out.print("*** DECONNECTION MySQL ***");
	  	connection.close();
	  	System.out.println("          [Ok]");
	}
	/**
	 * 
	 * @param strQuery
	 * @return ResultSet
	 * @throws SQLException
	 */
	public ResultSet execQuery(String strQuery) throws SQLException{
	  	System.out.println("*** Exécution de la requête "+strQuery+" ***");
        return statement.executeQuery(strQuery);
	}
	/**
	 * 
	 * @param strQueryUpdate
	 * @return int
	 * @throws SQLException
	 */
	public int execUpdate(String strQueryUpdate) throws SQLException{
        System.out.println("*** Exécution de la requête "+strQueryUpdate+" ***");
        return statement.executeUpdate(strQueryUpdate);
	}
	  
	/**
	 * Store une requête pour les listes en table
	 * @param query : requête à exécuter
	 */
	public void setQuery(String query){
		try {
			rs = statement.executeQuery(query);
		}
		catch (SQLException E){
			System.out.println("SQLException: " + E.getMessage());
			System.out.println("SQLState:     " + E.getSQLState());
			System.out.println("VendorError:  " + E.getErrorCode());
		}
	}
	/**
	 * 
	 * @return ResultSet
	 */
	public ResultSet getResultSet(){
		return rs;
	}

	/**
	 * Remplace des chaînes de caractères
	 * @param s : chaîne à modifier
	 * @param f : chaîne à remplacer
	 * @param r : chaîne de remplacement
	 * @return : chaîne modifiée
	 */
	public static String replace( String s, String f, String r ){
	     if (s == null)  return s;
	     if (f == null)  return s;
	     if (r == null)  r = "";

	     int index01 = s.indexOf( f );
	     while (index01 != -1)
	     {
	        s = s.substring(0,index01) + r + s.substring(index01+f.length());
	        index01 += r.length();
	        index01 = s.indexOf( f, index01 );
	     }
	     return s;
	}
}
