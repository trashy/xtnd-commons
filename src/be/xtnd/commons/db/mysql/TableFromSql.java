/*
 * TableFromSql.java, 2005-04-30
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               TableFromSql.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.AbstractTableModel;

/**
 * @author Johan Cwiklinski
 * @since 2005-04-30
 */
@Deprecated
public class TableFromSql extends AbstractTableModel {
	private static final long serialVersionUID = 4615050486958567057L;
	/** */
	public MySql connexion;
	/**
	 * 
	 * @param query
	 */
	public TableFromSql(String query){
		//connexion = MainGui.mysql;
		connexion = new MySql();
		connexion.setQuery(query);
	}
	/**
	 * @return int
	 */
	public int getColumnCount(){
		int i = 0;
		try {
				i = connexion.getResultSet().getMetaData().getColumnCount();
		}
		catch(SQLException e){
				System.out.println(e);
		}
		return i;
	}
	/**
	 * @return int
	 */
	public int getRowCount(){
		int i = 0;
		try {
			ResultSet rs = connexion.getResultSet();
			rs.last();
			i = rs.getRow();
		}
		catch(SQLException e){
			System.out.println(e);
		}
		return i;
	}
	/**
	 * @param c
	 * @return String
	 */
	public String getColumnName(int c){
		String s = "";
		try {
			s = connexion.getResultSet().getMetaData().getColumnName(c + 1);
		}
		catch(SQLException e){
			System.out.println(e);
		}
		return s;
	}
	/**
	 * @param row
	 * @param column
	 * @return Object
	 */
	public Object getValueAt(int row,int column){
		Object o = "";
		try {
			ResultSet rs = connexion.getResultSet();
			rs.absolute(row + 1);
			o = rs.getObject(column + 1);
		}
		catch(SQLException e){
			System.out.println(e);
		}
		return o;
	}
	
	//private Object[][] data;
	//private Object[] colname;

}