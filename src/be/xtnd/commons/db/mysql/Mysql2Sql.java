/*
 * Mysql2Sql.java, 2006-04-02
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               Mysql2Sql.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db.mysql;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.Db2Sql;

/**
 * MySQL database exports
 * <p><b>Tested with MySQL 5.0</b> only, but should work 
 * with MySQL 4.1+</p>
 * 
 * @author Johan Cwiklinski
 * @since 2006-04-02
 */
public class Mysql2Sql extends Db2Sql {

	/**
	 * 
	 * @param props
	 */
	public Mysql2Sql(Properties props){
		super(props);
	}

	/**
	 * 
	 * @param database
	 */
	public Mysql2Sql(Database database){
		super(database);
	}
	
	/**
	 * 
	 * @param database
	 * @param structure
	 * @param data
	 */
	public Mysql2Sql(Database database, boolean structure, boolean data){
		super(database, structure, data);
	}
	
	@Override
	protected void getTables(){
        try {
        	ResultSet rs = db.execQuery("SHOW TABLES");
        	tables = new ArrayList<String>();
        	while(rs.next()){
        		tables.add(rs.getString(1));
        	}
		} catch (SQLException e) {
			logger.fatal("SHOW TABLES fails : "+e.getMessage());
		}		
	}
	
	@Override
	protected void getCreateTables(){
        Iterator<String> it = tables.iterator();
        while(it.hasNext()){
        	String table = (String)it.next();
			try {
				ResultSet t = db.execQuery("SHOW CREATE TABLE "+table);
				while(t.next())
					instructions.add(t.getString(2));
			} catch (SQLException e) {
				logger.fatal("SHOW CREATE TABLE "+table+" FAILS : "+e.getMessage());
			}
        }		
	}
	
    /** 
     * Main method takes arguments for connection (property file).
     * If none specified, the default one will be used.
     * @param args 
     */
    public static void main(String[] args) {
    	String path = new String();
    	
        if (args.length != 1) {
            path = "be/xtnd/commons/db/mysql/mysql.properties";
        }

        props = new Properties();
        try {
       		props.load(new FileInputStream(new File(ClassLoader.getSystemResource(path).toURI())));
            new Mysql2Sql(props);
        } catch (IOException e) {
			logger.error("Properties file "+path+" cannot be loaded : "+e);
		} catch (URISyntaxException e) {
			logger.error("Properties file "+path+" cannot be loaded : "+e);
		}
    }
}
