/*
 * ResultSetTableModelFactory.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               ResultSetTableModelFactory.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import be.xtnd.commons.db.Database;

/**
 * Create a <code>ResultSetTableModel</code> that could be displayed
 * in a <code>JTable</code> swing component.
 * 
 * @author Johan Cwiklinski
 * @since 2005-03-31
 * @version 1.0
 **/
public class ResultSetTableModelFactory {
    Database database;
    boolean oldMode = false; //convenience for old method, will be deleted
    
    /**
     * Default constructor
     * @deprecated such a constructor should always be based on an existing
     * database connection, so we limit number of database connections. Use
     * {@link #ResultSetTableModelFactory(Database)} instead.
     */
    @Deprecated
    public ResultSetTableModelFactory(){
    	database = new Database();
    	database.setConnectionParameters(
    			ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
    	oldMode = true;
    }
    
    
    /** 
     * Default constructor.
     * Builds a <code>RestultSetTableModel</code> with the
     * given database.
     * @param db the database to use   
     *  
     */
    public ResultSetTableModelFactory(Database db){
    	database = db;
    }
    
    /**
     * Send a query to the database, loads results in a
     * <code>ResultSet</code> and return a <code>ResultSetTableModel</code>
     * which allow results to be displayed into a <code>JTable</code>.
     * 
     * @param query query to execute
     * @return ResultSetTableModel
     * @throws SQLException
     **/
    public ResultSetTableModel getResultSetTableModel(String query)
        throws SQLException{
    	ResultSet r = null;

    	if(oldMode){//convenience for old method, will be deleted
    		database.connect(); 
    		r = database.execQuery(query);
    	}else{
    		Statement stmt = database.getConnection().createStatement(
    				ResultSet.TYPE_SCROLL_INSENSITIVE,
    				ResultSet.CONCUR_READ_ONLY);
    		r = database.execQuery(query, stmt);
    	}
    	return new ResultSetTableModel(r);
    }
}
