/*
 * SqlErrors.java, 2006-05-26
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               SqlErrors.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db;

import be.xtnd.commons.gui.MainGui;

/**
 * @author Johan Cwiklinski
 * @since 2006-05-26
 */
public abstract class SqlErrors {
	protected static MainGui parent;
	private int unknown_host;
	private int unknown_database;
	private int access_denied;
	private int launched_once;
	private int duplicate;
	private int fk_fails;
	private int table_exists;

	/**
	 * 
	 * @param unknown_host
	 * @param unknown_database
	 * @param access_denied
	 * @param launched_once
	 * @param duplicate
	 * @param fk_fails
	 * @param table_exists
	 */
	public SqlErrors(
			int unknown_host,
			int unknown_database,
			int access_denied,
			int launched_once,
			int duplicate,
			int fk_fails,
			int table_exists){
		this.unknown_host = unknown_host;
		this.unknown_database = unknown_database;
		this.access_denied = access_denied;
		this.launched_once = launched_once;
		this.duplicate = duplicate;
		this.fk_fails = fk_fails;
		this.table_exists = table_exists;
	}
	
	/**
	 * @param i
	 * @return boolean
	 */
	public boolean isUnknownHost(int i){
		return i == this.unknown_host;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isUnknownDatabase(int i){
		return i == this.unknown_database;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isAccessDenied(int i){
		return i == this.access_denied;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isLaunchedOnce(int i){
		return i == this.launched_once;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isDuplicate(int i){
		return i == this.duplicate;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isFkFails(int i){
		return i == this.fk_fails;
	}
	
	/**
	 * 
	 * @param i
	 * @return boolean
	 */
	public boolean isExistingTable(int i){
		return i == this.table_exists;
	}
}
