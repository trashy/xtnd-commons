/*
 * Db2Sql.java, 2006-04-05
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               Db2Sql.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.db.mysql.Mysql2Sql;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.commons.security.Cryptographie;

/**
 * @author Johan Cwiklinski
 * @since 2006-04-05
 */
public abstract class Db2Sql {
	protected Database db;
	private boolean structure, data;
	
	protected ArrayList<String> tables;
	protected ArrayList<String> instructions;
	protected ArrayList<String> datas;

    /** Mode export sql */
    public static String SQL_MODE = "sql";
    /** Mode export xml */
    public static String XML_MODE = "xml";
    
    protected static Logger logger = Logger.getLogger(Mysql2Sql.class.getName());
    protected static Properties props;

	/**
	 * 
	 * @param props
	 */
	public Db2Sql(Properties props){
		this(new Database(
				props.getProperty("driver.name"),
				props.getProperty("driver.url"),
				props.getProperty("user"),
				props.getProperty("password"),
				props.getProperty("driver.class")
				),
			new Boolean(props.getProperty("save.strucutre")),
			new Boolean(props.getProperty("save.data"))
		);
	}

	/**
	 * 
	 * @param database
	 */
	public Db2Sql(Database database){
		this(database, true, true);
	}
	
	/**
	 * 
	 * @param database
	 * @param structure
	 * @param data
	 */
	public Db2Sql(Database database, boolean structure, boolean data){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		this.db = database;		
		this.structure = structure;
		this.data = data;
		
        getTables();
        if(structure){
    		instructions = new ArrayList<String>();
        	getCreateTables();
        }
        if(data){
        	datas = new ArrayList<String>();
        	getData();
        }
	}
	
	/**
	 * Retrieve tables form database
	 *
	 */
	protected abstract void getTables();
	
	/**
	 * Gets the create table scripts for all tables
	 *
	 */
	protected abstract void getCreateTables();
	
	/**
	 * Dump the selected database to a .sql or .xml syntax
	 * @param mode xml | sql
	 * @return String
	 */
	public String dump(String mode){
		StringBuffer sb = new StringBuffer();
		
		//XML file header
		if(mode.equals(XML_MODE)){
			sb.append("<?xml version = '1.0' encoding = 'UTF-8'?>\n");
			sb.append("<config title=\"");
			sb.append(CommonsI18n.tr("Backups datas"));
			sb.append("\" date=\"");
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			sb.append(df.format(new Date()));
			sb.append("\">\n");
			
			//engine configuration
			sb.append("\t<sql_providers>\n");
			sb.append("\t\t<sql_provider name=\"" + MainGui.conf.getProvider_name() + 
					"\" port=\"" + MainGui.conf.getPort() + 
					"\" default=\"yes\" mode=\"" + MainGui.conf.getMode() + 
					"\" arguments=\"" + MainGui.conf.getArguments() + 
					"\">" + MainGui.conf.getSql_provider() + "</sql_provider>\n");
			sb.append("\t</sql_providers>\n");			
			sb.append("\t<application toversion=\"\" fromversion=\"\" name=\"\" />\n");
			sb.append("\t<user>" + MainGui.conf.getSql_user() + "</user>\n");
			sb.append("\t<password>" + Cryptographie.crypte(MainGui.conf.getSql_password()) + "</password>\n");
			sb.append("\t<host>" + MainGui.conf.getServer_name() + "</host>\n");
			sb.append("\t<database>" + MainGui.conf.getSql_db() + "</database>\n");

			//start instructions
			sb.append("\t<instructions>\n");
		}
		
		if(structure){
			Iterator<String> it = instructions.iterator();
			while(it.hasNext()){
				String current = (String)it.next();
				sb.append((mode.equals(SQL_MODE))?"":"\t\t<instruction>");
				sb.append(current);
				sb.append((mode.equals(SQL_MODE))?";\n\n":"</instruction>\n");			
			}
		}
		
		if(data){
			Iterator<String> it = datas.iterator();
			while(it.hasNext()){
				String current = (String)it.next();
				sb.append((mode.equals(SQL_MODE))?"":"\t\t<instruction>");
				sb.append(current);
				sb.append((mode.equals(SQL_MODE))?";\n":"</instruction>\n");
			}
		}
        
		//XML file end
		if(mode.equals(XML_MODE)){
			sb.append("\t</instructions>\n");
			sb.append("</config>\n");
		}
		
		return sb.toString();
	}

	/**
	 * Loads datas from database
	 *
	 */
    private void getData() {
    	StringBuffer result;
    	Iterator<String> it = tables.iterator();
    	while(it.hasNext()){
    		String tableName = (String)it.next();
    		tableName = tableName.toLowerCase();
    		try {
	            PreparedStatement stmt = db.getConnection().prepareStatement("SELECT * FROM "+tableName);
	            ResultSet rs = stmt.executeQuery();
	            ResultSetMetaData metaData = rs.getMetaData();
	            int columnCount = metaData.getColumnCount();

	            datas.add("DELETE FROM " + tableName);
	            while (rs.next()) {
	            	result = new StringBuffer();
	                result.append("INSERT INTO "+tableName+" VALUES (");
	                for (int i=0; i<columnCount; i++) {
	                    if (i > 0) {
	                        result.append(", ");
	                    }
	                    Object value = rs.getObject(i+1);
	                    if (value == null) {
	                        result.append("NULL");
	                    } else {
	                        String outputValue = value.toString();
	                        outputValue = outputValue.replaceAll("'","''");
	                        if(
	                        		value.getClass().getName().equals("java.lang.Boolean") || 
	                        		value.getClass().getName().equals("java.lang.Integer")){
	                        	result.append(outputValue);
	                        }else{
	                        	result.append("'"+outputValue+"'");
	                        }
	                    }
	                }
	                result.append(")");
	                datas.add(result.toString());
	            }
	            rs.close();
	            stmt.close();
	        } catch (SQLException e) {
	        	logger.error("Error dumping "+tableName+" : "+e);
	        }
    	}
    }
}
