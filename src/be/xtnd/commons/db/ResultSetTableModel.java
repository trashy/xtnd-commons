/*
 * ResultSetTableModel.java, 2005-05-31
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               ResultSetTableModel.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Implements <code>TableModel</code> interface from  
 * <code>ResultSet</code> object. A <code>JTable</code> should 
 * this way show a <code>ResultSet</code> content.
 * A "scrollable" JDBC 2.0 <code>ResultSet</code> is required,
 * it will we read only mode.
 * 
 * @author Johan Cwiklinski
 * @since 2005-05-31
 * @version 1.0
 **/
public class ResultSetTableModel implements TableModel {
    ResultSet results;             //ResultSet to interpret
    ResultSetMetaData metadata;    //Meta datas
    int numcols, numrows;          //Rows and columns count
	static Logger logger = Logger.getLogger(ResultSetTableModel.class.getName());


    /**
     * Constructor that initialize a <code>TableModel</code> from
     * a <code>ResutSet</code>
     * Used by @link ResultSetTableModelFactory that need to be used  
     * to obtain generated model. 
     * 
     * @param results <code>ResultSet</code>
     * @throws SQLException
     **/
    ResultSetTableModel(ResultSet results) throws SQLException {
    	PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
    	this.results = results;                 // store ResultSet
    	metadata = results.getMetaData();       // get meta datas
    	numcols = metadata.getColumnCount();    // how many columns?
    	results.last();
    	numrows = results.getRow();             // how many rows?
    }
    
    /** 
     * Called when on <code>TableModel</code> destruct.
     * Close Ferme <code>Statement</code> and <code>ResultSet</code>
     */
    public void close() {
    	try { results.getStatement().close(); }
    	catch(SQLException e) {};
    }

    /** Automatically close on garbage collector */
    protected void finalize() { close(); }

    /** 
     * Get column count
     * 
     * @return int column count
     */
    public int getColumnCount() { return numcols; }

    /**
     * Get rows count
     * 
     * @return int rows count
     */
    public int getRowCount() { return numrows; }

    /**
     * Column name according to <code>ResultSet</code>
     * meta data.
     * 
     * @param column column index
     * 
     * @return String column name 
     */
    public String getColumnName(int column) {
	try {
	    return metadata.getColumnLabel(column+1);
	} catch (SQLException e) { return e.toString(); }
    }

    /**
     * Get the column class
     * 
     * @param column column index
     * 
     * @return column class
     */
    public Class<?> getColumnClass(int column) {
    	return (getValueAt(0, column)==null)?String.class:getValueAt(0, column).getClass();
    }
    
    /**
     * Get cell value.
     * Warning: rows and columns index into <code>ResultSet</code>
     * starts at 1, those from <code>TableModel</code> starts at 0!
     * 
     * @param row row to evaluate
     * @param column column to evaluate
     * 
     * @return Object evaluated cell value
     **/
    public Object getValueAt(int row, int column) {
    	try {
    		results.absolute(row+1);
    		Object o = results.getObject(column+1); 
    		if (o == null) return null;       
    		else return o;
    	} catch (SQLException e) {
    		logger.error("SQLException in RSTableModel : "+e.getMessage());
    		return null;
		}
    }
    
    /**
     * Find a text value and returns its line number
     * @param s string to find
     * @param col in which column we want to search
     * @return found line number ; -1 if any
     */
    public int findString(String s, int col){
    	int anInt = -1;
    	try{
    		results.first();
	    	while(results.next()){
	    		if(results.getObject(col+1).toString().equals(s))
	    			return results.getRow();
	    	}
    	}catch(SQLException e){
    		logger.error("SQLException finding text value in RSTableModel : "+e.getMessage());    		
    	}
    	return anInt;
    }

    /**
     * We want non editable cells.
     * @param row line number
     * @param column col number
     * @return boolean always false
     */
    public boolean isCellEditable(int row, int column) { return false; }
    
    //Following methods are not needeed, since table is always read-only
    /**
     * @param value value to store
     * @param row line number
     * @param column column number
     */
    public void setValueAt(Object value, int row, int column) {}

    /**
     * @param l TableModelListener
     */
    public void addTableModelListener(TableModelListener l) {}

    /**
     * @param l TableModelListener
     */
    public void removeTableModelListener(TableModelListener l) {}
}
