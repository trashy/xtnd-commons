/*
 * HelpWindow.java, 2005-06-05
 * 
 * This file is part of xtnd-commons.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               HelpWindow.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.commons;

import java.awt.Dimension;
import java.awt.Point;
import java.beans.PropertyVetoException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.help.BadIDException;
import javax.help.HelpSet;
import javax.help.HelpSetException;
import javax.help.JHelp;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

/**
 * Displays help window
 * 
 * @author Johan Cwiklinski
 * @since 2005-06-05
 */
public class HelpWindow extends JInternalFrame{
	private static final long serialVersionUID = -4034930522338806800L;
	/** Window name */
	public static final String name = "aide";
	/** Window location */
	public static Point location;
	static Logger logger = Logger.getLogger(HelpWindow.class.getName());
	private JHelp helpViewer = null; 
	private String help_id;
	
	/**
	 * Show specific id from help instead of root page
	 * 
	 * @param id id to show
	 */
	public void setCurrentId(String id){
		this.help_id = id;
		helpViewer.setCurrentID(id);
	}
	
	/**
	 * Creates help window
	 *
	 * @param bundle ResourceBundle à lire
	 */
	public HelpWindow(ResourceBundle bundle){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		try {  
			ClassLoader cl = HelpWindow.class.getClassLoader();  
			URL url = HelpSet.findHelpSet(cl, bundle.getString("appli.help_file"));  
			helpViewer = new JHelp(new HelpSet(cl, url));  

			EscapeInternalFrame frame = new EscapeInternalFrame();
			Object[] args = {bundle.getString("appli.name")};
			frame.setTitle(CommonsI18n.tr("{0} Help", args));

			frame.getContentPane().add(helpViewer);
			Dimension dim = new Dimension(770,500);
			frame.setMinimumSize(dim);
			frame.setPreferredSize(dim);
			frame.setSize(dim);
			frame.pack();
			
			frame.setIconifiable(true);
			frame.setClosable(true);
			frame.setResizable(true);
			frame.setMaximizable(true);
			frame.setName(name);
			frame.setVisible(true);
			
			MainGui.desktop.add(frame);	
			frame.toFront();
			try {
				frame.setSelected(true);
			} catch (PropertyVetoException e) {}
			frame.setLocation(MainGui.centerOnDesktop(frame.getSize()));		
		}catch(HelpSetException e){
			logger.error("HelpSetException : file "+bundle.getString("appli.help_file")+" not found");
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					CommonsI18n.tr("Cannot initialize help system.\nHelp will be deactived."),
					CommonsI18n.tr("Help"),
					JOptionPane.OK_OPTION);
		}catch (BadIDException e){
			logger.warn("BadIdException : id " + this.help_id + " not found");
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					CommonsI18n.tr("Cannot find specified index in help files.\nYou can continue and ignore this error."),
					CommonsI18n.tr("Help"), 
					JOptionPane.INFORMATION_MESSAGE + JOptionPane.OK_OPTION);
		}catch (RuntimeException e) {  
			logger.fatal("RuntimeException !!" + e.getMessage());
			JOptionPane.showMessageDialog(MainGui.desktop, 
					CommonsI18n.tr("Seem that a system error occured.\nPease inform support."),
					CommonsI18n.tr("Help"), 
					JOptionPane.WARNING_MESSAGE + JOptionPane.OK_OPTION);
		}catch (Exception e){
			logger.error("Exception : "+e.getMessage());
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					CommonsI18n.tr("An unknow error has occured.\nPlease inform support."),
					CommonsI18n.tr("Help"), 
					JOptionPane.WARNING_MESSAGE + JOptionPane.OK_OPTION);
		}
	}
}